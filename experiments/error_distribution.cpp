#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "./sketch_utils.cpp"
#include "../data/dataset_selection.cpp"
#include "../count_min.cpp"
#include "../count_median.cpp"

using namespace std;
using namespace sketchUtils;

namespace errorDistExperiment {

    template<class CS>
    int run(const string &filepath, const vector<itemKey> &data, const map<string, string> &meta, unsigned int k) {
        ofstream outFile;
        outFile.open(filepath, ios::trunc);

        if (!outFile.is_open()) {
            std::cout << "ERROR: Could not open file to save output.\n";

            return 1;
        }

        for (auto t = 1; t <= 10; t++) {
            processStreamAndOutputEstimatesToCsv<CS>(outFile, data, t, k, meta);
        }

        return 0;
    }

}

using namespace errorDistExperiment;

int main(int argc, char **argv) {
    const auto N = 500000;
    const auto m = 50000;
    const auto s = 1.07;

    auto dataSet = data::cli(m, N, s, argc, argv);

    if (!dataSet)
        return 1;

    for (int i = 0; i < argc; ++i) {
        if (string(argv[i]) == "-min") {
            map<string, string> meta = {
                    {"title",   "Count-Min Error Distribution"},
                    {"dataset", "\"" + dataSet->getTitle() + "\""},
            };

            return run<CountMin>(
                    "../experiments/out/error-dist-count-min-" + dataSet->getSlug(),
                    dataSet->asVector(),
                    meta,
                    400
            );
        }

        if (string(argv[i]) == "-median") {
            map<string, string> meta = {
                    {"title",   "Count-Median Error Distribution"},
                    {"dataset", "\"" + dataSet->getTitle() + "\""},
            };

            return run<CountMedian>(
                    "../experiments/out/error-dist-count-median-" + dataSet->getSlug(),
                    dataSet->asVector(),
                    meta,
                    400
            );
        }
    }

    cerr << "Sketch type not provided (-min/-median)." << endl;

    return 1;
}