#include "count_tree.cpp"
#include "count_min.cpp"

#ifndef ALGORITHMS_COMPACT_COUNT_TREE
#define ALGORITHMS_COMPACT_COUNT_TREE

class CompactCountTree : public CountTree {
protected:
    void init_sketches(unsigned int t, unsigned int k) const override {
        sketches[0] = new CountMin(t, k);

        for (int i = 1; i <= levels; i++) {
            // Error probability 1/4: log2(1 / 1/4) = 2.
            sketches[i] = new CountMin(2, k);
        }
    }

public:
    CompactCountTree(unsigned int t, unsigned int k, float phi, unsigned int size)
    {
        configure(t, k, phi, size);
        init_sketches(t, k);
    }
};

#endif //ALGORITHMS_COMPACT_COUNT_TREE