#ifndef ALGORITHMS_SKETCH_UTILS
#define ALGORITHMS_SKETCH_UTILS

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <unordered_set>
#include "../streaming_algorithm.h"
#include "../count_exact.cpp"

using namespace std;

namespace sketchUtils {

    template <class CS>
    void processStreamAndOutputEstimatesToCsv(
            ofstream &outFile,
            const vector<itemKey>& data,
            unsigned int t,
            unsigned int k,
            const map<string, string>& extraMeta
    ) {
        map<string, string> meta = {
                {"N", to_string(data.size())},
                {"t", to_string(t)},
                {"k", to_string(k)},
        };

        meta.insert(extraMeta.begin(), extraMeta.end());

        outFile << "#";
        for (auto const& item : meta) {
            outFile << "," << item.first;
        }

        outFile << "\n";

        auto first = true;
        for (auto const& item : meta) {
            if (! first) {
                outFile << ",";
            } else {
                first = false;
            }

            outFile << item.second;
        }

        outFile << "\n";

        auto countExact = CountExact();
        auto countSketch = CS(t, k);

        for (const auto item : data) {
            countExact.update(item, 1);
            countSketch.update(item, 1);
        }

        outFile << "i,f,n\n";

        unordered_set<itemKey> seen;
        for (const auto item : data) {
            if (seen.count(item))
                continue;

            outFile << item << "," << countExact.query(item) << "," << countSketch.query(item) << "\n";

            seen.insert(item);
        }
    }

    template <class CS>
    void processStreamAndOutputEstimatesToCsv(
            ofstream &outFile,
            const vector<itemKey>& data,
            unsigned int t,
            unsigned int k
    ) {
        processStreamAndOutputEstimatesToCsv<CS>(outFile, data, t, k, {});
    }

}


#endif //ALGORITHMS_SKETCH_UTILS
