import sys
from functools import reduce
from math import sqrt, exp, log10
from operator import itemgetter

import matplotlib.backends.backend_pdf
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
import matplotlib.ticker as ticker

import sketch_utils

ERROR_EDGECOLOR = 'r'
ERROR_FACECOLOR = colors.to_rgba('r', .5)


def num(s):
    """
    Try to cast a string to an int or float if possible.
    :param s
    :return: string casted as int, float or the same string if both attempts fail.
    """
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s


def round_nearest_multiple(x, base):
    return int(base * round(x / base))


def countmin_params(t, k, f):
    """
    Computes epsilon and delta from k and t and the guarantee.

    This guarantee is in terms of the L1-norm. Elements that have an error of exactly the guarantee are considered
    within the guarantee. Since an error is integer, we can cast the guarantee to an int.
    """
    epsilon = 2 / k
    delta = pow(2, -t)

    return epsilon, delta, int(epsilon * sum(f)), '$\\varepsilon n$'


def countmedian_params(t, k, f):
    """
    Computes epsilon and delta from k and t and the guarantee.

    This guarantee is in terms of the L2-norm. Elements that have an error of exactly the guarantee are considered
    within the guarantee. Since an error is integer, we can cast the guarantee to an int.
    """
    epsilon = sqrt(exp(3) / k)
    delta = 2 / exp(t)

    return epsilon, delta, int(epsilon * np.linalg.norm(np.array(f))), '$\\pm \\varepsilon \\left\\Vert f \\right\\Vert_2$'


def generate_metatext(meta):
    ordering = [
        ('t', 't'),
        ('k', 'k'),
        ('epsilon', '\\varepsilon'),
        ('delta', '\\delta')
    ]

    text = ''
    for k, symbol in ordering:
        if k not in meta:
            continue

        value = np.round(meta[k], 5)

        text += f'${symbol} = {value}$, '

    # Remove last ", ".
    return text[:-2]


def create_bins(lim, binwidth, outlier_binwidth):
    lastbin = round_nearest_multiple(np.abs(lim), binwidth)
    bins = list(range(0, lastbin + 1, binwidth))
    # Create the bin for the outliers.
    bins.append(lastbin + outlier_binwidth)

    if lim < 0:
        # [:0:-1] excludes the first element (0) and reverses the list.
        return list(map(lambda b: -b, bins))[:0:-1]

    return bins


# Normalized coloring from https://matplotlib.org/gallery/statistics/hist.html.
def color_bins(counts, patches):
    fracs = counts / counts.max()
    norm = colors.Normalize(fracs.min(), fracs.max())
    for frac, patch in zip(fracs, patches):
        color = plt.cm.viridis(norm(frac))
        patch.set_facecolor(color)


def color_error_area(x):
    """
    Color an area of the graph with a red tint to indicate that bins in this area are "error" bins. If x >= 0 this area
    will extend to the right side of the graph. Otherwise it'll extend to the left side.
    :param x:
    :return:
    """
    left_lim, right_lim = plt.xlim()
    to = right_lim if x >= 0 else left_lim
    plt.axvspan(min(x, to), max(x, to), facecolor='r', alpha=0.15)
    plt.xlim(right=right_lim)


def color_bins_error(patches):
    for patch in patches:
        patch.set_facecolor(ERROR_FACECOLOR)
        patch.set_edgecolor(ERROR_EDGECOLOR)


def plot_guarantee_line(guarantee):
    # Save the current y-limit so that when we plot epsilon * ||f|| we can make go all the way from the bottom to the
    # top without empty space above it.
    _, ytop = plt.ylim()
    # Since the guarantee is a <= only elements with an error from guarantee + 1 (rounded down since errors are integer)
    # should be considered beyond the guarantee not elements exactly at guarantee.
    plt.vlines(x=guarantee, ymin=0, ymax=sys.maxsize, color=ERROR_EDGECOLOR, linestyle='--')
    plt.ylim(top=ytop)


def set_outlier_style(patch):
    patch.set_linestyle(':')
    patch.set_linewidth(1.2)


def compute_total_error(data, guarantee):
    sum = reduce(lambda sum, error: sum + (np.abs(error) > guarantee), data, 0)

    return np.round(sum / len(data) * 100, 3) if sum != 0.0 else 0.0


def plot(meta, estimates, f, get_sketchparams, includes_negative, draw_error_area=True):
    fig, ax = plt.subplots(figsize=(6, 2.5))

    errors = [ni - fi for ni, fi in zip(estimates, f)]
    max_error = max(errors)
    min_error = min(errors)

    N, t, k = itemgetter('N', 't', 'k')(meta)

    epsilon, delta, guarantee, guarantee_label = get_sketchparams(t, k, f)

    # slack is as far you should go to the right of the guarantee. E.g. if the guarantee is 100 and we have an error
    # probability (delta) of 30% we certainly expect that there should be some that have an error > 100. This parameter
    # is how far out from a 100 we should keep creating bins of size binwidth. Outliers beyond this point are accounted
    # for. See comment about outliers. The lastbin value is non-inclusive. E.g. if guarantee + slack = 110 only items
    # with error < 110 will be in the bins.
    #
    # Outliers: In order to create a final bin with the outliers that have an error >= guarantee + slack we need to use
    # a "hack" which effectively makes any outlier data have the same max value. E.g. if we have [1, 2, 300, 400] and
    # guarantee + slack + outlier_binwidth = 10 then we transform the data into [1, 2, 10, 10].
    #
    # outlier_binwidth is the width of the final bin created which is a bin for the outliers that have
    # an error >= guarantee + slack. This parameter allows us to effectively increase the width of the final bin to give
    # a visual cue that it is a special bin that contains a greater range than the other ones on the graph.
    #
    # Compute a "pretty" binwidth. log10(n) + 1 gives us the no. of digits of n. Subtracting 1 from that we get the no.
    # of zeros we want for our bin width. E.g. if n = 2134 we want binwidth to become 2000 or if n = 153 we want
    # binwidth to become 200. The second argument to round is the no. of digits we want. Negative means we'll get that
    # amount of zeros.
    limit = guarantee
    if not draw_error_area:
        limit = int(limit / 10)

    binwidth = int(round(limit / 50, -(int(log10(limit / 50)))))

    if includes_negative:
        binwidth *= 2

    # Add slack.
    limit += binwidth * 10

    outlier_binwidth = 7 * binwidth

    meta['epsilon'], meta['delta'] = epsilon, delta

    bins = create_bins(limit, binwidth, outlier_binwidth)

    if includes_negative:
        bins = create_bins(-limit, binwidth, outlier_binwidth) + bins

    # Create the histogram using the "hack" described above.
    counts, bins, patches = ax.hist(np.clip(errors, bins[0], bins[-1]), bins=bins, edgecolor='#333333')

    color_bins(counts, patches)

    # Find the index of the first bin that strictly contains elements that violate the guarantee.
    pguarantee_index = None
    nguarantee_index = None
    for i, bin in enumerate(bins):
        if pguarantee_index is None and bin > guarantee:
            pguarantee_index = i

        if bin < 0 and np.abs(bin) > guarantee:
            # We want to subtract 1. If bin = -35 and binwidth = 5. bin represents the bar [-35, -30). If the
            # guarantee is 26 we don't want the bar [-30, -25) since it includes elements with an error within the
            # guarantee but bin = -30 will be the last bin where |bin| > guarantee and we wanted the previous bar
            # represnted by -35.
            nguarantee_index = i - 1

        if pguarantee_index is not None and (not includes_negative or nguarantee_index is not None):
            break

    # If the majority of the bin potentially split by the guarantee line is beyond the guarantee, we also want to color
    # that red.
    if nguarantee_index is not None and np.abs(bins[nguarantee_index] + binwidth + guarantee) >= binwidth / 2:
        nguarantee_index += 1

    if pguarantee_index is not None and bins[pguarantee_index] - guarantee >= binwidth / 2:
        pguarantee_index -= 1

    if draw_error_area:
        plot_guarantee_line(guarantee)

        if includes_negative:
            plot_guarantee_line(-guarantee)

        ax.plot([], [], color=ERROR_EDGECOLOR, linestyle='--', label=guarantee_label)

    # Color the bins that are beyond the guarantee.
    if pguarantee_index is not None:
        color_bins_error(patches[pguarantee_index:])

    if includes_negative and nguarantee_index is not None:
        color_bins_error(patches[:nguarantee_index + 1])

    # Change the outlier bin style.
    set_outlier_style(patches[-1])

    if includes_negative:
        set_outlier_style(patches[0])

    # Color the part of the graph with the bins beyond the error limit.
    if draw_error_area:
        color_error_area(guarantee)

        if includes_negative:
            color_error_area(-guarantee)

    major_ticks_locations = np.arange(0, bins[-2] + 1, binwidth * 10)
    minor_ticks_locations = np.arange(0, bins[-2] + 1, binwidth)
    if includes_negative:
        major_ticks_locations = np.append(-major_ticks_locations[::-1], major_ticks_locations)
        minor_ticks_locations = np.append(-minor_ticks_locations[::-1], minor_ticks_locations)

    if max_error >= bins[-1]:
        major_ticks_locations = np.append(major_ticks_locations, [bins[-1]])

    if min_error <= bins[0]:
        major_ticks_locations = np.append([bins[0]], major_ticks_locations)

    ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks_locations))
    ax.xaxis.set_minor_locator(ticker.FixedLocator(minor_ticks_locations))

    def major_formatter(x, pos):
        fmt = lambda x: '%d' % x

        if max_error >= bins[-1] and pos == len(major_ticks_locations) - 1:
            return fmt(max_error)

        if includes_negative and min_error <= bins[0] and pos == 0:
            return fmt(min_error)

        return fmt(x)

    ax.xaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

    # Show percentages on the y-axis.
    ax.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=len(f), is_latex=True))

    ax.set_xlabel(f'$n_i - f_i$')
    fig.subplots_adjust(top=0.83)
    fig.suptitle(meta['dataset'])
    ax.set_title(f'({generate_metatext(meta)})', fontdict={'fontsize': 9})

    if draw_error_area:
        ax.legend(loc='upper right', framealpha=1)

    ax.annotate(f'Failure total: {compute_total_error(errors, guarantee)}%', xy=(.02, .88), xycoords='axes fraction',
                size=10, ha='left', va='bottom')

    ax.tick_params(axis='x', labelsize=8)

    return fig, ax


def countsketch(name, params):
    filename = f'error-dist-{name}'

    try:
        runs = sketch_utils.read(f'./out/{filename}')
    except FileNotFoundError:
        print(f'Could not find file {filename}')
        exit(1)

    plots = []

    for run in runs:
        fig, ax = plot(*run, *params)
        plots.append((fig, ax))

    max_ytop = 0
    for _, ax in plots:
        _, ytop = ax.get_ylim()
        max_ytop = max(max_ytop, ytop)

    pdf = matplotlib.backends.backend_pdf.PdfPages(f'./out/{filename}.pdf')
    for fig, ax in plots:
        ax.set_ylim(0, max_ytop + max_ytop * .15)
        pdf.savefig(fig, bbox_inches='tight')

    pdf.close()


if __name__ == '__main__':
    usage_message = 'You must provide the sketch type (-min/-median) and data set (e.g. -zipfian).'

    if len(sys.argv) != 3:
        print(usage_message)
        exit(1)

    if '-min' == sys.argv[1]:
        countsketch('count-min-' + sys.argv[2][1:], (countmin_params, False))
    elif '-median' == sys.argv[1]:
        countsketch('count-median-' + sys.argv[2][1:], (countmedian_params, True, False))
    else:
        print(usage_message)
        exit(1)
