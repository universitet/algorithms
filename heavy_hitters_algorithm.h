#include <vector>
#include "streaming_algorithm.h"

#ifndef ALGORITHMS_HEAVY_HITTERS_ALGORITHM_H
#define ALGORITHMS_HEAVY_HITTERS_ALGORITHM_H

class HeavyHittersAlgorithm : StreamingAlgorithm {
public:
    virtual std::vector<itemKey> heavyHitters() = 0;
};

#endif //ALGORITHMS_HEAVY_HITTERS_ALGORITHM_H
