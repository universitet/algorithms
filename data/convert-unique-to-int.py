import fileinput, ipaddress, sys

if(len(sys.argv) != 3):
    print("Usage: {} <input-file> <output-file>".format(sys.argv[0]))
    quit()

inputFile = sys.argv[1]
targetFile = sys.argv[2]

mappings = {}
counter = 1

with open(inputFile, "r") as fpIn:
    with open(targetFile, "w+") as fp:
        for _, line in enumerate(fpIn):
            item = line.strip()

            if len(item) > 0:
                key = mappings.get(item)

                if key is None:
                    key = counter
                    mappings[item] = key
                    counter += 1

                    if counter % 100000 == 0:
                        print("Counter: " + str( counter))

                fp.write(str(key))
                fp.write("\n")
