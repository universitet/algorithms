#ifndef ALGORITHMS_DATA_STREAM_H
#define ALGORITHMS_DATA_STREAM_H

#if !defined(DATA_STREAM_BUFFER_SIZE)
#define DATA_STREAM_BUFFER_SIZE 30000
#endif

#include <string>
#include <fstream>
#include <vector>
#include "../streaming_algorithm.h"

class DataStream {
protected:
    std::ifstream filestream;
    std::vector<itemKey> buffer[DATA_STREAM_BUFFER_SIZE];

    void read();
public:
    explicit DataStream(std::string filename);
    int next();
    bool hasNext();
    void close();
    std::vector<itemKey> toVector();
};


#endif //ALGORITHMS_DATA_STREAM_H
