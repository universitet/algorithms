#include <limits.h>
#include <cmath>
#include "gmock/gmock-matchers.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "test_utils.cpp"
#include "../count_tree.cpp"

using namespace std;
using namespace testing;
using namespace testUtils;

namespace {

    TEST(CountTreeTest, SingleHeavy) {
        auto ct = CountTree(.5, .000001f, 8);

        // With phi = 0.5 and ||f|| = 3 we get that a heavy hitter >= 0.5 * 3 = 1.5 so since f_1 = 3 it will be
        // heavy. Of course by definition we will always have that if the stream only has one distinct element that
        // element will be heavy.
        ct.update(1, 3);

        EXPECT_THAT(ct.heavyHitters(), ElementsAre(1));
    };

    TEST(CountTreeTest, MultipleHeavy) {
        // With ||f|| = 10 and phi = 0.2 an element must have a count of at least 2 to be heavy.
        auto ct = CountTree(.2, .000001f, 8);

        ct.update(0, 1);
        ct.update(1, 2);
        ct.update(3, 7);

        EXPECT_THAT(ct.heavyHitters(), UnorderedElementsAre(1, 3));
    };

    TEST(CountTreeTest, Empty) {
        auto ct = CountTree(.2, .000001f, 8);

        EXPECT_THAT(ct.heavyHitters(), IsEmpty());
    };

    TEST(CountTreeTest, NoHeavy) {
        auto ct = CountTree(.4, .000001f, 1024);

        // ||f|| = 10 and phi = 0.4 => count should be at least 4 to be heavy.
        for (int i = 0; i < 10; i++) {
            ct.update(i, 1);
        }

        EXPECT_THAT(ct.heavyHitters(), IsEmpty());
    };

}