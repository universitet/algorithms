#!/usr/bin/python
import os
import sys
import random
import socket
import struct
import numpy as np
from zipfian import randomized_zipfian


def usage():
    print("Usage: {} <filename> <num> <m> <s> [-binom] [-graph] [-no-save] [-overwrite]".format(sys.argv[0]))
    print(
        "Generates a zipfian (or binom) distribution containing <num> items in the interval 0 and <m>. By providing -graph a popup will be displayed for the zipfian distribution")
    quit()


num_arg = len(sys.argv)
if num_arg < 4:
    usage()

is_binom = True if "-binom" in sys.argv else False
show_graph = True if "-graph" in sys.argv else False
do_save = True if "-no-save" not in sys.argv else False
do_overwrite = True if "-overwrite" not in sys.argv else False

if (is_binom and "-binom" in sys.argv[0:4 + 1] or
        show_graph and "-graph" in sys.argv[0:4 + 1]):
    print("ERROR: Order of the parameters matter!")
    usage()
    quit()

filename = sys.argv[1]
n = int(sys.argv[2])
m = int(sys.argv[3])
s = float(sys.argv[4])
items_written = 0

if os.path.isfile(filename) and do_save and not do_overwrite:
    choice = input("File '" + filename + "' already exists. Do you want to overwrite it? [Y/n] ").lower()
    if choice not in ['yes', 'y', 'ye', '']:
        print("Aborting...")
        quit()


def generate_plot(pool):
    if not show_graph:
        return

    import matplotlib.pyplot as plt

    plt.title('Zipfian distribution (N={},M={})'.format(n, m))
    plt.xlabel('Item')
    plt.ylabel('Frequency')
    plt.hist(pool, bins=np.arange(1, m + 2))

    if do_save:
        plt.savefig(filename + '.png')

    if show_graph:
        plt.show()


if is_binom:
    item_pool = np.random.binomial(m, 0.5, size=n)
else:
    item_pool = randomized_zipfian(m, n, s)

    generate_plot(item_pool)


def pick_item():
    # socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff))) # Random valid IP
    return item_pool[items_written]


def continue_writing(fh):
    return items_written < n


if do_save:
    with open(filename, 'w+') as f:
        while continue_writing(f):
            f.write(str(pick_item()) + "\n")
            items_written += 1
