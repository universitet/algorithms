#include <cmath>
#include <utility>
#include <algorithm>
#include <climits>
#include "frequency_estimation_algorithm.h"
#include "utils/hash_function.h"
#include "utils/prime_hash.cpp"

#ifndef ALGORITHMS_COUNT_MIN
#define ALGORITHMS_COUNT_MIN

using namespace std;

class CountMin : public FrequencyEstimationAlgorithm {
protected:
    unsigned int t;
    unsigned int k;

    long long int **A;

    HashFunction **hashFunctions;

    virtual long long int getCountForBucket(unsigned int row, itemKey item, int count) {
        return count;
    }

public:
    CountMin(unsigned int t, unsigned int k) {
        this->t = t;
        this->k = k;

        // Initialize t x k counter matrix.
        A = new long long int *[t];
        for (unsigned int i = 0; i < t; i++) {
            A[i] = new long long int[k];

            for (unsigned int j = 0; j < k; j++) {
                A[i][j] = 0;
            }
        }

        hashFunctions = new HashFunction *[t];
        for (unsigned int i = 0; i < t; i++) {
            hashFunctions[i] = new PrimeHash(k);
        }
    }

    CountMin(float epsilon, float delta) :
            CountMin(static_cast<unsigned int>(ceil(log2(1.0 / delta))),
                     static_cast<unsigned int>(ceil(2.0 / epsilon))) {}

    ~CountMin() {
        // Destroy counter matrix.
        for (unsigned int i = 0; i < t; i++) {
            delete[] A[i];

            delete hashFunctions[i];
        }

        delete[] A;

        delete[] hashFunctions;
    }

    void update(itemKey item, int count) override {
        for (unsigned int i = 0; i < t; i++) {
            unsigned int hash = hashFunctions[i]->hash(item);

            A[i][hash] += getCountForBucket(i, item, count);
        }
    }

    long long int query(itemKey item) override {
        long long int n = LONG_MAX;

        for (unsigned int i = 0; i < t; i++) {
            unsigned int hash = hashFunctions[i]->hash(item);

            n = min(n, A[i][hash]);
        }

        return n;
    }
};

#endif //ALGORITHMS_COUNT_MIN
