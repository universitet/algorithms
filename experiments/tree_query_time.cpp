#include <iostream>
#include <functional>
#include "benchmark/benchmark.h"
#include "../utils/data_stream.cpp"
#include "../count_min.cpp"
#include "../count_tree.cpp"
#include "../compact_count_tree.cpp"
#include "../utils/random.cpp"
#include "../data/dataset_selection.cpp"

using namespace std;

namespace treeQueryTime {

    /**
     * Generate the t and k arguments for the Count Sketch algorithm.
     *
     * @param b
     */
    static void Args(benchmark::internal::Benchmark *b) {
        for (auto t = 2; t <= 20; t += 2) {
            b->Args({t, 4000});
        }
    }

    /**
     * Benchmark the Count Sketch algorithm update operation by going through a stream of data. What is timed is the
     * time it takes to process the entire stream and not each update.
     *
     * @tparam CS
     * @param state
     * @param data
     */
    template<class CS>
    void TimeComplexity(benchmark::State &state, const vector<itemKey> &itemsToQuery, itemKey max) {
        auto t = state.range(0);
        auto k = state.range(1);
        auto phi = 0.001f;

        // Save t and k in the output for easy access.
        state.counters.insert({{"t", t},
                               {"k", k}});

        // Create the Count Tree Sketch data structure.
        auto cs = CS(t, k, phi, max);

        // Iterate through the stream and update.
        for (const auto item : itemsToQuery) {
            cs.update(item, 1);
        }

        auto counter = 0;
        for (auto _ : state) {
            // Iterate through the stream and query.
            // TODO: Adjust. This starts at 7 seconds on my computer.
            for (int i = 0; i < 5000; i++)
                counter = cs.heavyHitters().size();
        }

        // Important: Ensure we use the counter, so it is not optimized away
        std::cout << "Found at most: " << counter << "\n";
    }

    /**
     * Register a benchmark with a Count Sketch algorithm.
     *
     * The experiment is conducted as follows. For each t = 1, ..., 10 a Count Sketch data structure is created and the
     * stream is processed. This is repeated 10 times and the median running time is used as the measure for that t.
     */
    template<class CS>
    void Register(const string &name, const vector<itemKey> &data, itemKey max) {
        benchmark::RegisterBenchmark(name.c_str(), &TimeComplexity<CS>, data, max)
                // We only want to process the stream 1 time.
                ->Iterations(1)
                ->Apply(Args)
                ->Repetitions(5)
                        // Prevents each individual repetition from showing up in the result.
                ->ReportAggregatesOnly(true);
    }
}

using namespace treeQueryTime;

int main(int argc, char **argv) {
    const auto m = 50000;
    const auto N = 500000;
    const auto s = 1.07;

    //const auto datasets = datasetSelection::getDatasets(m, N, s);
    auto dataset = data::prompt(m, N, s);

    //for(const auto& dataset : datasets)
    {
        Register<CompactCountTree>(((string) "Compact Count-Min Tree Sketch").append("(").append(dataset->getTitle()).append(")"), dataset->asVector(), dataset->getMax());
        Register<CountTree>(((string) "Count-Min Tree Sketch").append("(").append(dataset->getTitle()).append(")"), dataset->asVector(), dataset->getMax());
    }

    benchmark::Initialize(&argc, argv);
    // Run the benchmarks once to warm up everything and disregard the results of this first run.
    // benchmark::RunSpecifiedBenchmarks();
    benchmark::RunSpecifiedBenchmarks();
}