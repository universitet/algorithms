import numpy as np
from glob import glob
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import pandas as pd
from scipy.stats import linregress

OUT_DIR = './out/'


def pretty_number(number):
    return '{:,}'.format(number)


def round_measure(measure, decimals=2):
    def compute(data):
        res = np.round(measure(data), decimals)

        return pretty_number(int(res) if decimals == 0 else res)

    return compute


def zipfian_skew(data):
    counts = sorted_counts(data)

    return -linregress(np.log2(np.arange(1, counts.size + 1)), np.log2(counts)).slope


def norm(ord=2):
    def compute(data):
        counts = sorted_counts(data)

        return np.linalg.norm(counts, ord=ord)

    return compute


MEASURES = {
    r'Count/$\ell_1$-norm': round_measure(norm(1), 0),
    r'$\ell_2$-norm': round_measure(norm(), 0),
    'Distinct items': lambda data: pretty_number(np.unique(data).size),
    'Min/Max': lambda data: f'{pretty_number(data.min())}/{pretty_number(data.max())}',
    'Zipfian skew': round_measure(zipfian_skew),
    #'Mean': round_measure(np.mean),
    #'Median': lambda data: int(np.median(data)),
    #'Variance': round_measure(np.var),
    #'Std. dev.': round_measure(np.std),
    #'Skewness': round_measure(skew),
}


def analyze(data):
    """
    >>> analyze(np.array([1, 2000, 2, 2000, 2000, 2000, 2000, 2000, 3, 2000, 2000, 2000, 2000, 2000, 2000]))
    [15, 4, '1/2000', -1.5]
    """

    def print_compute(kv):
        name, measure = kv

        print(f'\tCalculating {name}...')

        return measure(data)

    return list(map(print_compute, MEASURES.items()))


def sorted_counts(data):
    _, counts = np.unique(data, return_counts=True)

    return np.sort(counts)[::-1]


def loglog_plot(ax, name, data):
    ax.set_title(name)

    counts = sorted_counts(data)

    ax.loglog(np.arange(1, counts.size + 1), counts, color='C0')


def top_plot(ax, name, data):
    top = 50

    ax.set_title(name)

    counts = sorted_counts(data)

    ax.bar(range(1, top + 1), counts[:top], color='C0')

    ax.yaxis.set_major_formatter(PercentFormatter(xmax=data.size))

    ax.set_xticks(np.arange(0, top + 1, step=10))
    ax.set_xlim(xmin=0)


def results_to_latex(results):
    """
    >>> print(results_to_latex({'Test': [15, 4, '1/2000', -1.5], 'Test 2': [15, 4, '1/2000', -1.5]}))
    """

    values = np.array(list(results.values()))

    columns = '|l' + '|c' * len(results.keys()) + '|'
    headings = ' & ' + ' & '.join(results.keys())
    body = ''

    for i, measure in enumerate(MEASURES.keys()):
        body += measure + ' & ' + ' & '.join(values[:, i]) + r'\\ \hline' + '\n'

    return r"""
        \begin{tabular}{""".strip() + columns + r"""} \hline
        """ + headings + r""" \\ \hline\hline
        """ + body + r"""
        \end{tabular}
    """.strip()


if __name__ == '__main__':
    plt.style.use('seaborn')

    results = {}
    files = glob('*.dat')

    figsize = (13, 2)
    top = .7
    wspace = .3
    loglog_fig, loglog_axes = plt.subplots(ncols=len(files), figsize=figsize)
    top_fig, top_axes = plt.subplots(ncols=len(files), figsize=figsize)
    loglog_fig.subplots_adjust(wspace=wspace, top=top)
    top_fig.subplots_adjust(wspace=wspace, top=top)
    for i, filename in enumerate(files):
        print(f'Processing {filename}...')
        name = filename.replace('_', ' ').replace('.dat', '').title().replace('Ctu', 'CTU')
        data = pd.read_csv(filename, header=None).values.flatten()
        results[name] = analyze(data)
        loglog_plot(loglog_axes[i], name, data)
        top_plot(top_axes[i], name, data)
        print()

    loglog_axes[0].set_ylabel('Frequency')

    for fig in [loglog_fig, top_fig]:
        fig.text(0.5, -0.1, 'Rank', ha='center')

    fontsize = 16
    loglog_fig.suptitle('log-log Plots of Data Sets', fontsize=fontsize)
    top_fig.suptitle('Top 50 Most Frequent Items', fontsize=fontsize)

    loglog_fig.savefig(OUT_DIR + 'data-loglog.pdf', bbox_inches='tight')
    top_fig.savefig(OUT_DIR + 'data-top.pdf', bbox_inches='tight')

    with open(OUT_DIR + 'DatasetsCharacteristics.tex', 'w') as out:
        out.write(results_to_latex(results))
