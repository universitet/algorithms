import csv
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
from scipy.stats import linregress
import re
import seaborn as sns


RESULTS_FILE_MEDIAN = './out/heavy-precision-count-median'
RESULTS_FILE_MIN = './out/heavy-precision-count-min-phi-0.0001'
OUTPUT_FILE = './out/heavy_precision_graph_vertical.pdf'

def plot(data):
    plt.style.use('seaborn')
    plt.figure(figsize=(10, 10))
    plt.grid(True)
    plt.title("Precision for Heap")
    plt.subplots_adjust(hspace=0.4)
    #plt.suptitle("Tree time complexity")
    plt.tight_layout()

    counter = 1
    for dataset_name, lines in data.items():
        for name, points in lines.items():

            if name == "uniform":
                continue

            plt.subplot(2,4,counter)
            plot_single(name, {name: points}, counter == 1)
            counter += 1

    plt.savefig(OUTPUT_FILE,
                #bbox_extra_artists=[xlabel, ylabel],*/
                bbox_inches='tight')
    plt.clf()

def plot_single(title, data, display_ylabel=True):
    """
    Plot the data an do regression.
    :param name: Title on the plot.
    :param data: Data to plot
    :return:
    """
    counter = 0
    for name, points in data.items():
        points = np.array(points)[0:17]

        # o = circle, - = solid line between points.
        plt.plot(points[:, 0], points[:, 1], "X-", label='$\\frac{\phi}{2}$-precision')
        plt.plot(points[:, 0], points[:, 2], "X-", label="$\phi$-precision")
        plt.xticks(points[:, 0])
        counter += 1

    title = title.split("(")[0]
    plt.ylim(bottom=0, top=103)
    plt.gca().xaxis.set_major_locator(plt.MaxNLocator(7))
    plt.gca().yaxis.set_major_locator(plt.MaxNLocator(11))

    fmtr = ticker.PercentFormatter(decimals=0)
    plt.gca().yaxis.set_major_formatter(fmtr)

    plt.xlabel('$t$')
    if display_ylabel:
        plt.ylabel('Precision')
    plt.title(title)
    plt.legend()

def analyze_file(filename):
    data = {}

    with open(filename) as results_file:
        results_reader = csv.reader(results_file)

        results_iter = iter(results_reader)

        # Loop through the first few lines of the file containing information about the computer until the line with
        # column labels is reached.
        for labels in results_iter:
            if labels[0] == 'DataSet':
                break

        # Create a label => index map so that it's possible to index into a row by label.
        keys = {k: v for v, k in enumerate(labels)}

        for row in results_iter:
            # Google Benchmark creates a row for several statistics such as mean and median. The name of the statistic
            # is part of the name of the benchmark. We only want to use the median.
            #if row[keys['name']].find('median') == -1:
            #    continue

            # The first part of the name is the actual name of the benchmark. For each benchmark we find we want to
            # store the data for that benchmark, so we make sure we have an initialized array for the data here.
            name = row[keys['DataSet']].split('/')[0]
            if name not in data:
                data[name] = []

            # Append the data entry with the x-value being the t and y being the time spent.
            data[name].append([
                int(row[keys['T']]),
                float(row[keys['Precision (within phi/2)']]) * 100,
                float(row[keys['Precision (within phi)']]) * 100
            ])

    return data

if __name__ == '__main__':

    data = {}
    data['Count-Min'] = analyze_file(RESULTS_FILE_MIN)
    #data['Count-Median'] = analyze_file(RESULTS_FILE_MEDIAN)

    plot(data)
