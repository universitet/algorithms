
#ifndef ALGORITHMS_HEAP_ITEM
#define ALGORITHMS_HEAP_ITEM

#include "../streaming_algorithm.h"

struct HeapItem
{
    itemKey key;
    unsigned int frq;

    HeapItem(itemKey item, unsigned int frequency) : key(item), frq(frequency) {}

    bool operator < (const HeapItem& b) const
    {
        return (frq < b.frq);
    }

    bool operator > (const HeapItem& b) const
    {
        return (frq > b.frq);
    }

    bool operator == (itemKey item) const
    {
        return (key == item);
    }

    bool operator == (HeapItem item) const
    {
        return (key == item.key);
    }
};

#endif //ALGORITHMS_HEAP_ITEM