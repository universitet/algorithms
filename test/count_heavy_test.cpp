#include <limits.h>
#include "gtest/gtest.h"
#include "../count_heavy.cpp"
#include "../count_min.cpp"
#include "test_utils.cpp"
#include "gmock/gmock-matchers.h"

using namespace std;
using namespace testUtils;

namespace {

    TEST(CountHeavyTest, EstimateOfSingleItemStream) {

        unsigned int elm1 = 1;
        unsigned int elm2 = 2;

        auto heap = CountHeavy<CountMin>(.5f, 0.000001f);

        heap.update(elm1, 2);
        heap.update(elm2, 10);
        heap.update(elm1, 5);

        EXPECT_THAT(heap.heavyHitters(), testing::ElementsAre(elm2));
    };

    TEST(CountHeavyTest, EstimateOfOnesInStream) {

            unsigned int elm1 = 1;
            unsigned int elm2 = 2;

            auto heap = CountHeavy<CountMin>(.5f, 0.000001f);

            // Elm1 = 7
            // Elm2 = 2
            heap.update(elm1, 1);
            heap.update(elm2, 1);
            heap.update(elm1, 1);
            heap.update(elm1, 1);
            heap.update(elm1, 1);
            heap.update(elm1, 1);
            heap.update(elm1, 1);
            heap.update(elm1, 1);
            heap.update(elm2, 1);

            EXPECT_THAT(heap.heavyHitters(), testing::ElementsAre(elm1));
    };

}