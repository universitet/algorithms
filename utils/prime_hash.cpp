#include "hash_function.h"
#include "random.cpp"
#include "../streaming_algorithm.h"

#ifndef ALGORITHMS_PRIME_HASH
#define ALGORITHMS_PRIME_HASH


class PrimeHash : public HashFunction {
private:
    // See comment in hash method about the choice of this prime.
    //
    // 2^32 - 5. This allows universes up to 2^32 - 6 although it really only matters slightly theoretically if we were
    // to actually hash items in a universe of size 2^32 - 1 which is the max value of an unsigned int.
    const unsigned int PRIME = 4294967291;

    // We store a and b as unsigned long long since we need to do calculations that might yield very large numbers.
    unsigned long long a;
    unsigned long long b;
    unsigned int k;

public:
    explicit PrimeHash(unsigned int k) {
        this->k = k;

        a = rand_int(1, PRIME);
        b = rand_int(0, PRIME);
    }

    unsigned int hash(itemKey item) override {
        // An unsigned int (itemKey) has a max value of 2^32 - 1. We want a * item + b <= 2^64 - 1 to be able to fit it
        // in an unsigned long long (unsigned 64-bit int). We also need the prime to be > 2^32 - 1 for the hash family
        // to be universal. Unfortunately both of these conditions cannot be satisfied if we allow items of size
        // 2^32 - 1 but we can get close. 2^32 - 5 is a prime (https://primes.utm.edu/lists/2small/0bit.html) so
        // although we'll violate the second condition by using it we will for the sake of simplicity. Of course this is
        // all just based on what the types allow. If we're just hashing items from a domain <= 2^32 - 6 in practice
        // everything holds.
        return (a * item + b) % PRIME % k;
    }
};

#endif //ALGORITHMS_PRIME_HASH