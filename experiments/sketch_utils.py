import csv
import numpy as np
import pandas as pd

def num(s):
    """
    Try to cast a string to an int or float if possible.
    :param s
    :return: string casted as int, float or the same string if both attempts fail.
    """
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s


def read(filepath):
    runs = []

    with open(filepath, 'r') as file:
        reader = csv.reader(file)
        names = range(len(next(reader)))

    results = pd.read_csv(filepath, header=None, index_col=False, names=names).values
    results_iter = iter(results)

    meta_labels = [next(results_iter)]

    for ml in meta_labels:
        meta = dict(zip(ml[1:], map(num, next(results_iter))))
        estimates = []
        f = []

        next(results_iter)
        for row in results_iter:
            if row[0] == '#':
                meta_labels.append(row)
                break

            estimates.append(int(row[2]))
            f.append(int(row[1]))

        runs.append((meta, np.array(estimates), np.array(f)))

    return runs
