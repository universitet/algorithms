#include <map>
#include <string>
#include <iostream>
#include "frequency_estimation_algorithm.h"

#ifndef ALGORITHMS_COUNT_EXACT
#define ALGORITHMS_COUNT_EXACT

using namespace std;

class CountExact : public FrequencyEstimationAlgorithm {
protected:
    map<int, int> counter;

public:
    void update(itemKey item, int count) override {
        counter[item] += count;
    }

    long long int query(itemKey item) override {
        return counter[item];
    }

    void displayFrequencies() {
        map<int, int>::iterator itr;
        cout << "\nExact counter: \n";
        cout << "\tELEMENT\t\t\tCOUNT\n";
        for (itr = counter.begin(); itr != counter.end(); ++itr) {
            cout << '\t' << itr->first
                 << "\t\t" << itr->second << '\n';
        }
        cout << endl;
    }
};

#endif //ALGORITHMS_COUNT_EXACT
