#ifndef ALGORITHMS_DATASETS
#define ALGORITHMS_DATASETS

#include "../utils/vformat.cpp"
#include "../utils/random.cpp"
#include "zipfian.cpp"

using namespace utils;

namespace data {

    class Dataset {
    private:
        vector<itemKey> data;

    public:
        virtual ~Dataset() = default;

        virtual string getSlug() = 0;

        virtual string getTitle() = 0;

        virtual itemKey getMax() = 0;

        virtual vector<itemKey> asVector() {
            if (!data.empty()) {
                return data;
            }

            data = DataStream("../data/" + getSlug() + ".dat").toVector();

            return data;
        }
    };

    class Zipfian : public Dataset {
    private:
        vector<itemKey> data;
        unsigned int m;
        unsigned int N;
        float s;

    public:
        Zipfian(unsigned int m, unsigned int N, float s) : m(m), N(N), s(s) {}

        string getSlug() override {
            return "zipfian";
        }

        string getTitle() override {
            return vformat("Zipfian, $n = %u$, $m = %u$, $s = %.2f$", N, m, s);
        }

        itemKey getMax() override {
            return m;
        }

        vector<itemKey> asVector() override {
            if (!data.empty()) {
                return data;
            }

            data = data::randomizedZipfian(m, N, s);

            return data;
        }
    };

    class StaticZipfian : public Dataset {
    private:
        unsigned int m = 10000;
        unsigned int N = 100000;
        float s = 1.07;

    public:
        StaticZipfian() {}

        string getSlug() override {
            return "zipfian";
        }

        string getTitle() override {
            return vformat("Zipfian, $n = %u$, $m = %u$, $s = %.2f$", N, m, s);
        }

        itemKey getMax() override {
            return m;
        }
    };

    class Kosarak : public Dataset {
        string getSlug() override {
            return "kosarak";
        }

        string getTitle() override {
            return "Kosarak";
        }

        itemKey getMax() override {
            return 41270;
        }
    };

    class Newsgroups : public Dataset {
        string getSlug() override {
            return "20_newsgroups";
        }

        string getTitle() override {
            return "20 Newsgroups";
        }

        itemKey getMax() override {
            return 101630;
        }
    };

    class Ctu13 : public Dataset {
        string getSlug() override {
            return "ctu-13";
        }

        string getTitle() override {
            return "CTU-13";
        }

        itemKey getMax() override {
            return 542077;
        }
    };

    class Uniform : public Dataset {
    private:
        vector<itemKey> data;
        unsigned int m;
        unsigned int N;
    public:
        Uniform (unsigned int m, unsigned int N) : m(m), N(N) {}

        string getSlug() override {
            return "uniform";
        }

        string getTitle() override {
            return vformat("Uniform, $n = %u$, $m = %u$", N, m);
        }

        itemKey getMax() override {
            return m;
        }

        vector<itemKey> asVector() override {
            if (!data.empty()) {
                return data;
            }

            for (auto i = 0; i < N; i++) {
                data.push_back(rand_int(1, m));
            }

            return data;
        }
    };

}


#endif //ALGORITHMS_DATASETS
