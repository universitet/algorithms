#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <math.h>
#include "../utils/vformat.cpp"
#include "./sketch_utils.cpp"
#include "../data/datasets.cpp"
#include "../count_min.cpp"
#include "../count_median.cpp"

using namespace std;
using namespace sketchUtils;

namespace sizeExperiment {

    template <class CS>
    int run(const string &filepath, const vector<itemKey> &data) {
        ofstream outFile;
        outFile.open(filepath, ios::trunc);

        if (!outFile.is_open()) {
            std::cout << "ERROR: Could not open file to save output.\n";

            return 1;
        }

        const auto size = 2500;

        for (auto t = 3; t <= 20; t++) {
            processStreamAndOutputEstimatesToCsv<CS>(outFile, data, t, round(size / t));
        }

        return 0;
    }

}

using namespace sizeExperiment;
using namespace data;

int main() {
    const auto N = 100000;
    const auto m = 10000;
    const auto ss = { .5, 1., 1.5, 2. };

    for (auto s : ss) {
        auto data = Zipfian(m, N, s).asVector();
        auto minRes = run<CountMin>(vformat("../experiments/out/size-count-min-s-%.1f", s), data);
        auto medianRes = run<CountMedian>(vformat("../experiments/out/size-count-median-s-%.1f", s), data);

        if (minRes != 0 || medianRes != 0) return 1;
    }

    return 0;
}