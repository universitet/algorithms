import fileinput, ipaddress, sys

if(len(sys.argv) != 2):
    print("Usage: {} <input-file>".format(sys.argv[0]))
    quit()

targetFile = "ip-firewall-integers";

with open(targetFile, "w+") as fp:
    for line in fileinput.input():
        try:
            if(len(line.strip()) > 0):
                fp.write(str(int(ipaddress.IPv4Address(line.strip()))))
                fp.write("\n")
        except:
            pass