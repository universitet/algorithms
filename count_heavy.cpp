#include <cmath>
#include "heavy_hitters_algorithm.h"
#include "utils/min_heap.cpp"
#include "frequency_estimation_algorithm.h"

#ifndef ALGORITHMS_COUNT_HEAVY
#define ALGORITHMS_COUNT_HEAVY

using namespace std;

template <class T>
class CountHeavy : public HeavyHittersAlgorithm, public FrequencyEstimationAlgorithm {
private:
    unsigned long long totalCount = 0;
    float epsilon;

protected:
    T* countAlgorithm;
    MinHeap heap;
public:
    // For CountExact
    explicit CountHeavy(float epsilon) : epsilon(epsilon), heap(ceil(2.0f / epsilon)) {
        countAlgorithm = new T();
    }

    CountHeavy(float epsilon, float delta) : epsilon(epsilon), heap(ceil(2.0f / epsilon)) {
        countAlgorithm = new T(epsilon, delta);
    }

    CountHeavy(float epsilon, unsigned int t, unsigned int k) : epsilon(epsilon), heap(ceil(2.0f / epsilon)) {
        countAlgorithm = new T(t, k);
    }

    ~CountHeavy() {
        delete this->countAlgorithm;
    }

    void update(itemKey item, int count) override {
        totalCount += count;
        countAlgorithm->update(item, count);
        heap.put(item, countAlgorithm->query(item));
    }

    unsigned long long norm(){
        return totalCount;
    }

    long long int query(itemKey item) override {
        return countAlgorithm->query(item);
    }

    vector<itemKey> heavyHitters() override {
        vector<itemKey> heavy = {};

        for (auto const item : heap.getItemKeys())
        {
            if (countAlgorithm->query(item) >= epsilon * totalCount) {
                heavy.push_back(item);
            }
        }

        return heavy;
    }
};

#endif //ALGORITHMS_COUNT_HEAVY
