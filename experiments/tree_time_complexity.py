import csv
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
import re

RESULTS_FILE_KOSARAK = './out/tree-time-complexity-kosarak'
RESULTS_FILE_NEWSGROUP = './out/tree-time-complexity-newsgroups'
RESULTS_FILE_CTU = './out/tree-time-complexity-ctu'
RESULTS_FILE_ZIPFIAN = './out/tree-time-complexity-zipfian'

plt.style.use('seaborn')

def plot(data):

    plt.figure(figsize=(6, 2.5))
    plt.grid(True)
    plt.title("Time Complexity for Trees")
    #plt.suptitle("Tree time complexity")
    plt.tight_layout()

    counter = 1
    for dataset_name, lines in data.items():
        plt.subplot(1,2,counter)
        plot_single(dataset_name, lines, counter==1)
        counter += 1

    plt.gcf().legend(ncol=2, loc=8)
    plt.savefig('./out/tree_time_precision_graph.pdf',
                #bbox_extra_artists=[xlabel, ylabel],*/
                bbox_inches='tight')
    plt.clf()

def plot_single(title, data, include_label=True):
    """
    Plot the data an do regression.
    :param name: Title on the plot.
    :param data: Data to plot
    :return:
    """
    styles = ['C1X-','C2X-']
    i = 0
    for name, points in data.items():
        points = np.array(points)
        # o = circle, - = solid line between points.
        plt.plot(points[:, 0], points[:, 1] / 1000, styles[i], label=(format_legend(name) if include_label else ""))
        plt.xticks(points[:, 0])
        i += 1

    title = title.split("(")[0]

    plt.xlabel('$t$')

    if include_label:
        plt.ylabel('Time in s')
    plt.title(title)
    #plt.legend()

def format_legend(text):
    if "Compact" in text:
        return "Compact"
    else:
        return "Regular"

def analyze_file(filename):
    data = {}

    with open(filename) as results_file:
        results_reader = csv.reader(results_file)

        results_iter = iter(results_reader)

        # Loop through the first few lines of the file containing information about the computer until the line with
        # column labels is reached.
        for labels in results_iter:
            if labels[0] == 'name':
                break

        # Create a label => index map so that it's possible to index into a row by label.
        keys = {k: v for v, k in enumerate(labels)}

        for row in results_iter:
            # Google Benchmark creates a row for several statistics such as mean and median. The name of the statistic
            # is part of the name of the benchmark. We only want to use the median.
            if row[keys['name']].find('median') == -1:
                continue

            # The first part of the name is the actual name of the benchmark. For each benchmark we find we want to
            # store the data for that benchmark, so we make sure we have an initialized array for the data here.
            name = row[keys['name']].split('/')[0]
            if name not in data:
                data[name] = []

            # Append the data entry with the x-value being the t and y being the time spent.
            data[name].append([int(row[keys['t']]), float(row[keys['real_time']]) * pow(10, -6)])

    return data

if __name__ == '__main__':

    data = {}
    data['Kosarak'] = analyze_file(RESULTS_FILE_KOSARAK)
    data['20 Newsgroups'] = analyze_file(RESULTS_FILE_NEWSGROUP)
    #data['CTU-13'] = analyze_file(RESULTS_FILE_CTU)
    #data['Zipfian'] = analyze_file(RESULTS_FILE_ZIPFIAN)

    plot(data)



