//
// Created by Morten on 14-03-2019.
//
#include <vector>
#include "heap_item.cpp"

#ifndef ALGORITHMS_HEAP_H
#define ALGORITHMS_HEAP_H

class Heap {
protected:
    int size = 0;
    int maxSize = 0;
    std::vector<HeapItem> items;
public:
    virtual void insert(itemKey item, unsigned int count) = 0;
    virtual void update(int index, unsigned int count) = 0;
    virtual int getSize() = 0;
};

#endif //ALGORITHMS_HEAP_H
