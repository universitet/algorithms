#include <chrono>
#include <random>

#ifndef ALGORITHMS_RANDOM
#define ALGORITHMS_RANDOM

// random number generator from Stroustrup:
// http://www.stroustrup.com/C++11FAQ.html#std-random
int rand_int(unsigned int low, unsigned int high)
{
    static std::default_random_engine re(std::random_device{}());
    using Dist = std::uniform_int_distribution<unsigned int>;
    static Dist uid {};
    return uid(re, Dist::param_type{low, high});
}

#endif //ALGORITHMS_RANDOM