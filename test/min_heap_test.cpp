#include <limits.h>
#include <cmath>
#include "gmock/gmock-matchers.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "../utils/heap_item.cpp"
#include "../utils/min_heap.cpp"

using namespace std;
using namespace testing;

namespace {

    TEST(MinHeapTest, Size) {
        auto heap = MinHeap(2);

        heap.put(1, 3);
        heap.put(2, 10);

        EXPECT_EQ(heap.getSize(), 2);
    };

    TEST(MinHeapTest, SizeLimited) {
        auto heap = MinHeap(2);

        heap.put(1, 3);
        heap.put(2, 10);
        heap.put(3, 10);

        EXPECT_EQ(heap.getSize(), 2);
    };

    TEST(MinHeapTest, SameElement) {
        auto heap = MinHeap(10);

        heap.put(1, 3);
        heap.put(1, 12);
        heap.put(2, 20);

        EXPECT_EQ(heap.getSize(), 2);
        EXPECT_EQ(heap.getRoot()->frq, 15);
    };

    TEST(MinHeapTest, UpdateElements) {
        auto heap = MinHeap(10);

        heap.put(1, 3); // New root
        heap.put(1, 5);
        EXPECT_EQ(heap.getRoot()->frq, 8);

        heap.put(2, 2); // New root
        EXPECT_EQ(heap.getRoot()->frq, 2);

        heap.put(2, 20); // New root is now 1
        EXPECT_EQ(heap.getRoot()->frq, 8);
    };

    TEST(MinHeapTest, UpdateRootElement) {
        auto heap = MinHeap(1);

        heap.put(1, 3); // New root
        EXPECT_EQ(heap.getRoot()->frq, 3);

        heap.put(2, 2); // Wont be inserted
        EXPECT_EQ(heap.getRoot()->frq, 3);

        heap.put(2, 20); // New root is now 2 => 20
        EXPECT_EQ(heap.getRoot()->frq, 20);

        heap.put(1, 19); // Wont be inserted
        EXPECT_EQ(heap.getRoot()->frq, 20);
    };

    TEST(MinHeapTest, Sorting5Items) {
        auto heap = MinHeap(10);

        heap.put(1, 10);
        heap.put(2, 100);
        heap.put(3, 30);
        heap.put(4, 20);
        heap.put(5, 5);

        EXPECT_EQ(heap.getSize(), 5);
        EXPECT_EQ(heap.getRoot()->frq, 5);
        heap.print(); // Manuel test
    };

    TEST(MinHeapTest, ReturningElements) {
        auto heap = MinHeap(3);

        heap.put(1, 10);
        heap.put(2, 100);
        heap.put(3, 30);
        heap.put(4, 20);
        heap.put(5, 5);
        heap.put(6, 40);
        heap.put(7, 120);

        EXPECT_EQ(heap.getSize(), 3);
        EXPECT_EQ(heap.getRoot()->frq, 40);
        EXPECT_THAT(heap.getItemKeys(), UnorderedElementsAre(2, 6, 7));
    };

    TEST(HeapItem, MultipleItemsDifferentFrq){
        auto item1 = HeapItem(1, 10);
        auto item2 = HeapItem(3, 20);

        EXPECT_EQ(item1.frq, 10);
        EXPECT_EQ(item2.frq, 20);
    };

    TEST(HeapItem, ComparableSelf){
        auto item = HeapItem(1, 30);

        EXPECT_FALSE(item < item);
        EXPECT_FALSE(item > item);
    };

    TEST(HeapItem, ComparableLess){
        auto item1 = HeapItem(1, 30);
        auto item2 = HeapItem(2, 40);
        auto item3 = HeapItem(3, 10);

        EXPECT_TRUE(item1 < item2);
        EXPECT_TRUE(item3 < item1);
        EXPECT_TRUE(item3 < item2);
    };

    TEST(HeapItem, ComparableMore){
        auto item1 = HeapItem(1, 30);
        auto item2 = HeapItem(2, 40);
        auto item3 = HeapItem(3, 10);

        EXPECT_TRUE(item1 > item3);
        EXPECT_TRUE(item2 > item1);
        EXPECT_TRUE(item2 > item3);
    };

    TEST(HeapItem, ComparableEqual){
        auto item1 = HeapItem(1, 30);
        auto item2 = HeapItem(2, 40);

        EXPECT_TRUE(item2 == 2);
        EXPECT_FALSE(item1 == 2);
    };
}