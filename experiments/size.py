from glob import glob
import sketch_utils
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


def plot(ax, runs, fmt):
    x, y = [], []
    for meta, estimates, f in runs:
        t, k = meta['t'], meta['k']

        x.append(t)
        y.append(np.array([np.abs(ni - fi) for ni, fi in zip(estimates, f)]).mean())

    ax.plot(x, y, fmt, markersize=5)


if __name__ == '__main__':
    plt.style.use('seaborn')

    min_fmt = 'C1X-'
    median_fmt = 'C2X-'

    min_files, median_files = sorted(glob('./out/size-count-min-*')), sorted(glob('./out/size-count-median-*'))

    no_s = min(len(min_files), len(median_files))
    fig, axes = plt.subplots(ncols=no_s, figsize=(3 * no_s, 3))
    for min_file, median_file, ax in zip(min_files, median_files, axes):
        print(min_file, median_file)
        plot(ax, sketch_utils.read(min_file), min_fmt)
        plot(ax, sketch_utils.read(median_file), median_fmt)

        ax.set_title(f'$s = {min_file[-3:]}$')
        ax.xaxis.set_major_locator(ticker.MultipleLocator(base=2))

    plt.plot([], [], min_fmt, label='Count-Min')
    plt.plot([], [], median_fmt, label='Count-Median')

    fig.legend(*plt.gca().get_legend_handles_labels(), ncol=2, loc='upper center')

    fig.savefig('./out/size.pdf', bbox_inches='tight')
