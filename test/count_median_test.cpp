#include <limits.h>
#include "gtest/gtest.h"
#include "../count_median.cpp"
#include "test_utils.cpp"

using namespace std;
using namespace testUtils;

namespace {

TEST(CountMedianTest, EstimateOfSingleItemStream) {
    // t = 1, k = 3
    auto cm = CountMedian(1u, 3u);

    cm.update(1, 2);
    cm.update(1, 5);

    EXPECT_EQ(cm.query(1), 7);
};

TEST(CountMedianTest, EstimateOfItemNotInStream) {
    // t = 1, k = 3
    auto cm = CountMedian(1u, 3u);

    EXPECT_EQ(cm.query(1), 0);
}

TEST(CountMedianTest, EstimateOfElementsInTinyStream) {
    const float epsilon = .1f;
    vector<int> frequencies = {5, 5, 3, 4, 9, 23, 99, 1, 42};

    // delta = 0.000001 => 99.999999% probability that the estimates should be in range of the threshold.
    auto cm = CountMedian(epsilon, 0.000001f);

    for (int i = 0; i < frequencies.size(); i++) {
        cm.update(i, frequencies[i]);
    }

    for (int i = 0; i < frequencies.size(); i++) {
        ASSERT_PRED4(estimateInRange, frequencies[i], epsilon, calcL1Norm(frequencies), cm.query(i));
    }
};

}