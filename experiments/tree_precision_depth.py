import csv
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
from scipy.stats import linregress
import re
import sys


RESULTS_FILE = './results/tree-compare-compact'
plt.style.use('seaborn')

def plot_depth(lines, title, from_zero=True, include_legend=False, include_label=False):
    styles = ['C1X-','C2X-']
    i = 0
    for label, data in lines.items():
        data = np.array(data)

        plt.xticks(data[:, 0])
        plt.gca().xaxis.set_major_locator(plt.MaxNLocator(8))


        # k = black, o = circle, - = solid line between points.
        if include_legend:
            plt.plot(data[:, 0], data[:, 1], styles[i], label=label)
        else:
            plt.plot(data[:, 0], data[:, 1], styles[i])

        if i == 0 and from_zero:
            ymin = (np.min(data[:, 1]) if not from_zero else 0)
            ymax = np.max(data[:, 1])
            t_calc = np.log2(4/data[0,-1])
            #plot_vertical_line(t_calc, '$log(4/\phi)$', ymin)

            plt.ylim(top=ymax*1.1)

        if from_zero:
            plt.ylim(bottom=0) # Hardcoded top
            plt.xlabel('$t$')

        if include_label:
            plt.ylabel('Nodes visited')
        plt.title(title)
        i+=1
    # plt.legend()

def plot_combined(data):
    plt.figure(figsize=(13, 4))
    plt.grid(True)
    plt.tight_layout()

    i = 1
    n = len(datasets)

    for name, lines in datasets.items():
        plt.subplot(2,n,i)
        plot_depth(lines, format_dataset_name(name), False, (i == 1), (i == 1))
        i += 1

    for name, lines in datasets.items():
        plt.subplot(2,n,i)
        plot_depth(lines, "", True, include_label=(i%n==1))
        i += 1

    plt.gcf().legend(ncol=2, loc='lower center')
    plt.savefig('./out/depth-combined.pdf', bbox_inches='tight')
    plt.clf()

def format_dataset_name(name):

    name = name.replace("_", " ").capitalize().replace("Ctu", "CTU").replace("news", "News")

    return name

if __name__ == '__main__':
    datasets = {}

    with open(RESULTS_FILE) as results_file:
        results_reader = csv.reader(results_file)

        results_iter = iter(results_reader)

        # Loop through the first few lines of the file containing information about the computer until the line with
        # column labels is reached.
        for labels in results_iter:
            print("Label: ", labels)
            break


        # Create a label => index map so that it's possible to index into a row by label.
        keys = {k: v for v, k in enumerate(labels)}

        for row in results_iter:
            dataset = row[keys['DataSet']]
            type = row[keys['Algo']]

            if dataset not in datasets:
                datasets[dataset] = {'Regular': [],'Compact': []}

            point = [float(row[keys[k]]) for k in ['T','Depth','Precision (within phi)','Precision (within phi/2)','Recall','Phi']]

            datasets[dataset][type].append(point)

    plot_combined(datasets)


