#include "../count_tree.cpp"
#include "../compact_count_tree.cpp"
#include "../count_min.cpp"
#include "../count_heavy.cpp"
#include "../count_median.cpp"
#include "../count_exact.cpp"
#include "../utils/data_stream.cpp"
#include "../data/dataset_selection.cpp"
#include "../data/zipfian.cpp"
#include "benchmark.cpp"
#include <functional>

using namespace std;

const auto datasets = {
        vector<itemKey>()
        /*data::randomizedZipfian(5, 20, 1.07),
        data::randomizedZipfian(20, 200, 1.07),
        data::randomizedZipfian(20, 2000, 1.07),
        data::randomizedZipfian(10, 20000, 1.07),
        data::randomizedZipfian(20, 20000, 1.07),
        data::randomizedZipfian(1000, 100000, 1.07),
        data::randomizedZipfian(100, 1000000, 1.07)*/
};
const itemKey datasets_max = 1;


struct result {
    double precision;
    double upper_precision;
    double recall;
    unsigned long realHeavy;
    unsigned long foundHeavy;
};


template<class T>
void runData(T &algorithm, const vector<itemKey> &dataset) {
    for (const auto item : dataset)
        algorithm.update(item, 1);
}

template<class T>
void runData(CountHeavy<T> &algorithm, const vector<itemKey> &dataset) {
    for (const auto item : dataset)
        algorithm.update(item, 1);
}

template<class T>
result compareHeavy(T &estimator, CountHeavy<CountExact> &real, double epsilon) {
    auto realHeavy = real.heavyHitters();
    auto foundHeavy = estimator.heavyHitters();

    //printf("Number of real heavy hitters: %ld\n", realHeavy.size());
    //printf("Number of estimated heavy hitters: %ld\n", foundHeavy.size());

    //if (foundHeavy.empty() || realHeavy.empty())
    //    return result{0, 0};

    unsigned int norm = real.norm();
    auto correctHeavy = 0;
    auto correctSemiHeavy = 0;

    for (auto const &value: foundHeavy) {
        if (real.query(value) >= epsilon * norm) {
            correctHeavy++;
        } else if (real.query(value) >= (epsilon / 2) * norm) {
            correctSemiHeavy++;
        } else {
            //printf("Wrong heavy: %d\n", value);
        }
    }

    auto res = result{0, 0};
    if(!foundHeavy.empty()) {
        res.precision = (double) (correctHeavy + correctSemiHeavy) / (double) foundHeavy.size();
        res.upper_precision = (double) correctHeavy / (double) foundHeavy.size();
    }

    res.recall = correctHeavy / (double) realHeavy.size();
    res.realHeavy = realHeavy.size();
    res.foundHeavy = foundHeavy.size();

    return res;
}

void printResult(result res) {
    printf("Real heavy: %ld\nFound heavy: %ld\nPrecision (wihtin phi/2)op: %f\nPrecision (within phi): %f\nRecall: %f\n",
           res.realHeavy,
           res.foundHeavy,
           res.precision,
           res.upper_precision,
           res.recall);
}

/**
 * Runs the provided algorithm along with CountHeavy<CountExact> on all the datasets
 * and returns the precision and recall on each individual dataset
 *
 * @param CountHeapEstimator
 * @param epsilon
 */
vector<result> runTests(CountTree &countHeapEstimator, float epsilon) {
    auto countExact = CountHeavy<CountExact>(epsilon);
    vector<result> results = vector<result>();

    for (const auto &dataset : datasets) {
        runData(countHeapEstimator, dataset);
        runData(countExact, dataset);

        results.push_back(compareHeavy(countHeapEstimator, countExact, epsilon));
    }

    return results;
}

void writeCsvRow(ofstream &outFile, vector<string> values) {
    outFile << values[0];
    int cols = values.size();
    for (int i = 1; i < cols; i++)
        outFile << "," << values[i];
    outFile << "\n";
}

void test1() {
    auto epsilon = 0.01f;
    auto cMin = CountTree(epsilon, 0.0000001f, datasets_max);
    auto cExact = CountHeavy<CountExact>(epsilon);
    const auto dataset = data::randomizedZipfian(10, 20000, 1.07);

    runData(cMin, dataset);
    runData(cExact, dataset);

    auto result = compareHeavy(cMin, cExact, epsilon);
    printResult(result);
}

void test2() {
    auto epsilon = 0.01f;
    auto cMin = CountTree(epsilon, 0.0000001f, datasets_max);

    auto results = runTests(cMin, epsilon);

    for (const auto &result: results) {
        printResult(result);
        printf("\n");
    }
}

void testCompact() {
    auto phi = 0.001f;
    auto t = 1;
    unsigned int k = ceil(4 / phi);

    auto cMin = CompactCountTree(t, k, phi, 1234);
    cMin.update(1234, 1);
}

/**
 * t, k and phi are only provided for logging. Returns true if phi-precision is 100% (not phi/2-precision)
 *
 * @tparam T type of Count Heap
 * @param outFile
 * @param countHeapEstimator
 * @param real
 * @param data
 * @param t
 * @param k
 * @param phi
 * @return
 */
template<class T>
bool runAndStoreResultsInCsv(
        ofstream &outFile,
        const string &algo,
        const string &dataset,
        T &countHeapEstimator,
        CountHeavy<CountExact> &countExact,
        const vector<itemKey> &data,
        unsigned int t,
        unsigned int k,
        float phi) {
    runData(countHeapEstimator, data);

    auto result = compareHeavy(countHeapEstimator, countExact, phi);

    writeCsvRow(outFile, vector<string>{
            dataset,
            algo,
            to_string(t),
            to_string(k),
            to_string(phi),
            to_string(result.realHeavy),
            to_string(result.foundHeavy),
            to_string(result.precision),
            to_string(result.upper_precision),
            to_string(result.recall),
            to_string(countHeapEstimator.nodesVisited())
    });

    return (result.upper_precision == 1.0 && result.recall == 1.0);
}

/*template<class T>
void test3(const string& algo) {
    auto phi = 0.001f;

    ofstream outFile;
    outFile.open(string("../experiments/out/tree-precision-").append(algo), ios::trunc);

    if (!outFile.is_open()) {
        std::cout << "ERROR: Could not open file to save output.\n";
        return;
    }
    writeCsvRow(outFile, vector<string>{
            "DataSet",
            "T",
            "K",
            "Phi",
            "Real Heavy",
            "Estimated Heavy",
            "Precision (within phi/2)",
            "Precision (within phi)",
            "Recall"
    });

    unsigned int k, t;
    bool fullPrecision;

    // Loop over datasets
    vector<pair<string, std::function<vector<itemKey>()>>> dataSets = {
            make_pair(
                    "Kosarak",
                    []() { return DataStream("../data/kosarak.dat").toVector(); }
            ),
            make_pair(
                    "20 Newsgroups",
                    []() { return DataStream("../data/20_newsgroups.dat").toVector(); }
            ),
            make_pair(
                    "CTU-13",
                    []() { return DataStream("../data/ctu-13.dat").toVector(); }
            )
    };

    for (const auto& datapair : dataSets) {
        printf("Running on dataset %s\n", datapair.first.c_str());
        fflush(stdout);

        auto data = datapair.second();
        auto cExact = CountHeavy<CountExact>(phi);
        runData(cExact, data);

        k = ceil(4 / phi);
        for (int i = 1; i < 15; i++) {
            t = i;

            auto cMin = CountTree(t, k, phi);
            fullPrecision = runAndStoreResultsInCsv(outFile, datapair.first, cMin, cExact, data, t, k, phi);

            if (fullPrecision)
                break;
        }
    }
    outFile.close();
}
*/

void testCompareTrees() {
    auto phi = 0.001f;

    ofstream outFile;
    outFile.open("../experiments/out/tree-compare-compact", ios::trunc);

    if (!outFile.is_open()) {
        std::cout << "ERROR: Could not open file to save output.\n";
        return;
    }
    writeCsvRow(outFile, vector<string>{
            "DataSet",
            "Algo",
            "T",
            "K",
            "Phi",
            "Real Heavy",
            "Estimated Heavy",
            "Precision (within phi/2)",
            "Precision (within phi)",
            "Recall",
            "Depth"
    });

    unsigned int k, t;
    bool fullPrecision, fullPrecision2;

    auto dataSets = datasetSelection::getDatasets(50000, 500000, 1.07);

    for (const auto& dataset : dataSets) {
        printf("Running on dataset %s\n", dataset->getTitle().c_str());
        fflush(stdout);

        auto data = dataset->asVector();
        auto cExact = CountHeavy<CountExact>(phi);
        runData(cExact, data);

        k = ceil(4 / phi);

        for (int i = 1; i <= 20; i++) {
            t = i;

            auto cMin = CountTree(t, k, phi, dataset->getMax());
            fullPrecision = runAndStoreResultsInCsv<CountTree>(outFile, "Regular", dataset->getSlug(), cMin, cExact, data, t, k, phi);

            auto cMinCompact = CompactCountTree(t, k, phi, dataset->getMax());
            fullPrecision2 = runAndStoreResultsInCsv<CompactCountTree>(outFile, "Compact", dataset->getSlug(), cMinCompact, cExact, data, t, k, phi);

            //if (fullPrecision && fullPrecision2)
            //    break;
        }
    }
    outFile.close();
}



int main() {
    //test3<CountMin>("count-min");
    testCompareTrees();
    //testCompact();

    return 0;
}

