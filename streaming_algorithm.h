#ifndef ALGORITHMS_STREAMING_ALGORITHM_H
#define ALGORITHMS_STREAMING_ALGORITHM_H

using itemKey = unsigned int;

class StreamingAlgorithm {
public:
    virtual void update(itemKey item, int count) = 0;
};

#endif //ALGORITHMS_STREAMING_ALGORITHM_H
