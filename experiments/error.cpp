#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "./sketch_utils.cpp"
#include "../data/dataset_selection.cpp"
#include "../count_min.cpp"
#include "../count_median.cpp"

using namespace std;
using namespace sketchUtils;

namespace errorExperiment {

    template<class CS>
    int run(const string &filepath, const shared_ptr<Dataset> &dataSet, unsigned int k) {
        ofstream outFile;
        outFile.open(filepath, ios::trunc);

        if (!outFile.is_open()) {
            std::cout << "ERROR: Could not open file to save output.\n";

            return 1;
        }

        map<string, string> meta = {
                {"title", "\"" + dataSet->getTitle() + "\""},
        };

        for (auto t = 1; t <= 20; t++) {
            processStreamAndOutputEstimatesToCsv<CS>(outFile, dataSet->asVector(), t, k, meta);
        }

        return 0;
    }

}

using namespace errorExperiment;

int main(int argc, char **argv) {
    const auto N = 500000;
    const auto m = 50000;
    const auto s = 1.07;

    auto dataSet = data::cli(m, N, s, argc, argv);

    if (!dataSet)
        return 1;

    auto minRes = run<CountMin>("../experiments/out/error-count-min-" + dataSet->getSlug(), dataSet, 400);
    auto medianRes = run<CountMedian>("../experiments/out/error-count-median-" + dataSet->getSlug(), dataSet, 400);

    return minRes != 0 || medianRes != 0;
}