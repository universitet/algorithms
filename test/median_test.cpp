#include <limits.h>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iostream>
#include "gtest/gtest.h"
#include "../utils/median.cpp"


using namespace std;


TEST(MedianTest, MedianOfEvenNumbers) {
    auto pairs = {
            make_pair(vector<long long int>{5}, 5),
            make_pair(vector<long long int>{10, 20}, 15),
            make_pair(vector<long long int>{10, 20, 80}, 20),
            make_pair(vector<long long int>{10, 20, 10, 20}, 15),
            make_pair(vector<long long int>{10, 20, 50, 60}, 35),
            make_pair(vector<long long int>{10, 21, 50, 60}, 36)
    };

    for(auto data : pairs)
    {
        EXPECT_EQ( utils::median( data.first ), data.second) ;
    }
}