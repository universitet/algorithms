import csv
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
from scipy.stats import linregress
import re
import sys


RESULTS_FILE = './out/tree-compare-compact'
plt.style.use('seaborn')

def plot_depth(lines, title, from_zero=True):
    styles = ['C1X-','C2X-']
    i = 0
    for label, data in lines.items():
        data = np.array(data)

        plt.xticks(data[:, 0])
        plt.gca().xaxis.set_major_locator(plt.MaxNLocator(8))


        # k = black, o = circle, - = solid line between points.
        plt.plot(data[:, 0], data[:, 1], styles[i], label=label)

        if i == 0 and from_zero:
            ymin = (np.min(data[:, 1]) if not from_zero else 0)
            t_calc = np.log2(4/data[0,-1])
            #plot_vertical_line(t_calc, '$log(4/\phi)$', ymin)

        if from_zero:
            plt.ylim(bottom=0) # Hardcoded top

        plt.xlabel('$t$')
        plt.ylabel('Nodes visited')
        plt.title(title)
        i+=1
    plt.legend()

def plot_precision(lines, title, from_zero=True):
    styles = ['C1X-','C2X-']
    i = 0
    for label, data in lines.items():
        data = np.array(data)

        plt.xticks(data[:, 0])
        plt.gca().xaxis.set_major_locator(plt.MaxNLocator(8))

        # k = black, o = circle, - = solid line between points.
        plt.plot(data[:, 0], data[:, 3], styles[i], label=label)

        if i == 0 and from_zero:
            ymin = (np.min(data[:, 1]) if not from_zero else 0)
            t_calc = np.log2(4/data[0,-1])
            #plot_vertical_line(t_calc, '$log(4/\phi)$', ymin)

        if from_zero:
            plt.ylim(bottom=0, top=1.1)

        plt.xlabel('$t$')
        plt.ylabel('Precision ($\phi$)')
        plt.title(title)
        i+=1

    plt.legend()

def plot_vertical_line(x_val, label, ymin=0):
    # Save the current y-limit so that when we plot epsilon * ||f|| we can make go all the way from the bottom to the
    # top without empty space above it.
    _, ytop = plt.ylim()
    # Since the guarantee is a <= only elements with an error from guarantee + 1 (rounded down since errors are integer)
    # should be considered beyond the guarantee not elements exactly at guarantee.
    plt.vlines(x=x_val, ymin=ymin, ymax=sys.maxsize, color='paleturquoise', linestyle='--', label=label)
    plt.ylim(top=ytop)

def plot(name, lines):
    """
    Plot the data an do regression.
    :param name: Title on the plot.
    :param data (dict): Data to plot
    :return:
    """
    plt.figure(figsize=(10, 4))
    plt.grid(True)
    plt.suptitle(format_dataset_name(name))
    plt.tight_layout()

    # Relative graphs
    plt.subplot(2,2,1)
    plot_depth(lines, "", False)
    plt.subplot(2,2,2)
    plot_precision(lines, "", False)

    # Absolute graphs
    plt.subplot(2,2,3)
    plot_depth(lines, "", True)
    plt.subplot(2,2,4)
    plot_precision(lines, "", True)

    plt.subplots_adjust(top=0.9)
    # Enforce tick display
    #x_values = np.array(lines["Regular"])[:,0]
    #plt.xticks(np.arange(min(x_values), max(x_values)+1, 5.0))

    title = name
    if name.find('('):
        meta = name[name.find('(') + 1:-1]
        dataset_name = re.search("[a-zA-Z]+", meta)[0]
        title = name[:name.find('(')] + dataset_name

    plt.savefig('./out/compact-' + title.lower().replace(' ', '-').replace('\n', '-') + '.pdf',
                #bbox_extra_artists=[xlabel, ylabel],
                bbox_inches='tight')
    plt.clf()

def format_dataset_name(name):

    name = name.replace("_", " ").capitalize().replace("Ctu", "CTU")

    return name

if __name__ == '__main__':
    datasets = {}

    with open(RESULTS_FILE) as results_file:
        results_reader = csv.reader(results_file)

        results_iter = iter(results_reader)

        # Loop through the first few lines of the file containing information about the computer until the line with
        # column labels is reached.
        for labels in results_iter:
            print("Label: ", labels)
            break


        # Create a label => index map so that it's possible to index into a row by label.
        keys = {k: v for v, k in enumerate(labels)}

        for row in results_iter:
            dataset = row[keys['DataSet']]
            type = row[keys['Algo']]

            if dataset not in datasets:
                datasets[dataset] = {'Regular': [],'Compact': []}

            point = [float(row[keys[k]]) for k in ['T','Depth','Precision (within phi)','Precision (within phi/2)','Recall','Phi']]

            datasets[dataset][type].append(point)

    for label, lines in datasets.items():
        plot(label, lines)


