from abc import ABC, abstractmethod
from math import sqrt, exp
from glob import glob
from multiprocessing import Pool
from collections import namedtuple
import sketch_utils
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

SketchParams = namedtuple('SketchParams', ['epsilon', 'delta', 'guarantee', 'type'])

GUARANTEE_META = {
    'min': {'linestyle': 'g--', 'label': r'$\varepsilon_{\mathrm{min}} \left\Vert f \right\Vert$'},
    'median': {'linestyle': 'r--', 'label': r'$\varepsilon_{\mathrm{med}} \left\Vert f \right\Vert_2$'}
}


def countmin_params(t, k, f):
    epsilon = 2 / k
    delta = pow(2, -t)

    return SketchParams(epsilon, delta, epsilon * np.linalg.norm(f, ord=1), 'min')


def countmedian_params(t, k, f):
    epsilon = sqrt(exp(3) / k)
    delta = 2 / exp(t)

    return SketchParams(epsilon, delta, epsilon * np.linalg.norm(f), 'median')


class AccuracyMeasure(ABC):
    label = f'Missing Label'

    def __init__(self, compute_params):
        super().__init__()

        self.compute_params = compute_params

    @abstractmethod
    def __call__(self, *args):
        pass

    def adjust_plot(self, ax):
        pass

    def compute_errors(self, f, estimates):
        return np.array([np.abs(ni - fi) for ni, fi in zip(estimates, f)])


class TotalAccuracy(AccuracyMeasure):
    label = 'Total Accuracy'

    def adjust_plot(self, ax):
        ax.set_ylim(bottom=.90, top=1.01)
        ax.yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1))

    def __call__(self, meta, estimates, f):
        params = self.compute_params(meta['t'], meta['k'], f)

        return (self.compute_errors(f, estimates) <= params.guarantee).mean()


class MeanErrorMeasure(AccuracyMeasure):
    label = 'Mean Error'

    def adjust_plot(self, ax):
        ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    def __call__(self, meta, estimates, f):
        return self.compute_errors(f, estimates).mean()


class MedianErrorMeasure(AccuracyMeasure):
    label = 'Median Error'

    def adjust_plot(self, ax):
        ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    def __call__(self, meta, estimates, f):
        return np.median(self.compute_errors(f, estimates))


class NNNPercentileMeasure(AccuracyMeasure):
    label = '99.9 Percentile Error'

    def adjust_plot(self, ax):
        ax.set_yscale('log')
        ax.locator_params(axis='y', tight=True, numticks=4)

    def __call__(self, meta, estimates, f):
        return np.percentile(self.compute_errors(f, estimates), 99.9)


class DeltaPercentileMeasure(AccuracyMeasure):
    label = '$1 - \delta$ Percentile Error'

    def __init__(self, compute_params):
        super().__init__(compute_params)

    def adjust_plot(self, ax):
        left, right = ax.get_xlim()
        ax.plot([left, right], [self.guarantee] * 2, GUARANTEE_META[self.guarantee_type]['linestyle'], linewidth=1)
        ax.set_xlim((left, right))
        ax.set_yscale('log')
        ax.locator_params(axis='y', tight=True, numticks=4)

    def __call__(self, meta, estimates, f):
        params = self.compute_params(meta['t'], meta['k'], f)

        self.guarantee = params.guarantee
        self.guarantee_type = params.type

        return np.percentile(self.compute_errors(f, estimates), 100 * (1 - params.delta))


def compute_points(measure, runs):
    x, y = [], []
    for meta, estimates, f in runs:
        t, k = meta['t'], meta['k']

        x.append(t)
        y.append(measure(meta, estimates, f))

    return x, y


def plot(ax, measure, points, fmt):
    ax.plot(*points, fmt, markersize=5)

    ax.xaxis.set_major_locator(ticker.MultipleLocator(base=2))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(base=1))
    measure.adjust_plot(ax)


def read_file(file):
    print(f'Reading "{file}"')
    return sketch_utils.read(file)


MEASURES = [MeanErrorMeasure, NNNPercentileMeasure, DeltaPercentileMeasure]

if __name__ == '__main__':
    plt.style.use('seaborn')

    all_files = {'min': [], 'median': []}
    for type in all_files.keys():
        all_files[type] = sorted(glob(f'./out/error-count-{type}-*'))

    no_datasets = len(all_files['min'])
    p = Pool(no_datasets * 2)

    all_runs = {}
    for type, files in all_files.items():
        all_runs[type] = p.map_async(read_file, files)

    for type, res in all_runs.items():
        all_runs[type] = res.get()

    fig, axes = plt.subplots(
        ncols=no_datasets,
        nrows=len(MEASURES),
        figsize=(3 * no_datasets, 2 * len(MEASURES)),
        squeeze=False,
        sharex=True
    )

    for type, all_sketch_runs in all_runs.items():
        if type == 'min':
            compute_params = countmin_params
            fmt = 'C1X-'
        else:
            compute_params = countmedian_params
            fmt = 'C2X-'

        for i, Measure in enumerate(MEASURES):
            for j, dataset_runs in enumerate(all_sketch_runs):
                measure = Measure(compute_params)
                plot(axes[i, j], measure, compute_points(measure, dataset_runs), fmt)

        plt.plot([], [], fmt, label=f'Count-{type.capitalize()} Sketch')

    for meta in GUARANTEE_META.values():
        plt.plot([], [], meta['linestyle'], label=meta['label'])

    lgd = fig.legend(*plt.gca().get_legend_handles_labels(), ncol=4, loc='upper center')

    for ax, all_min_runs in zip(axes[0, :], all_runs['min']):
        title = all_min_runs[0][0]['title']
        if ',' in title:
            title = title[:title.find(',')]

        ax.set_title(title)

    for ax, Measure in zip(axes[:, 0], MEASURES):
        ax.set_ylabel(Measure.label)

    fig.align_ylabels(axes[:, 0])

    for ax in axes[-1, :]:
        ax.set_xlabel('$t$')

    fig.subplots_adjust(top=(1 - .32 / len(MEASURES)))
    fig.savefig('./out/error.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
