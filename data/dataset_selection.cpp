#ifndef ALGORITHMS_DATASET_SELECTION
#define ALGORITHMS_DATASET_SELECTION

#include <vector>
#include <memory>
#include "datasets.cpp"

using namespace std;
using namespace data;

namespace datasetSelection {

    vector<shared_ptr<Dataset>> getDatasets(unsigned int m, unsigned int N, float s) {
        return {
                make_shared<Zipfian>(m, N, s),
                make_shared<Kosarak>(),
                make_shared<Newsgroups>(),
                make_shared<Ctu13>(),
                make_shared<Uniform>(m, N),
        };
    }

    vector<shared_ptr<Dataset>> getDatasets() {
        // Same params as the static zipfian
        unsigned int m = 10000;
        unsigned int N = 100000;
        return {
                make_shared<Kosarak>(),
                make_shared<Newsgroups>(),
                make_shared<Ctu13>(),
                make_shared<StaticZipfian>(),
                make_shared<Uniform>(m, N),

        };
    }
}

using namespace datasetSelection;

namespace data {

    shared_ptr<Dataset> prompt(unsigned int m, unsigned int N, float s) {
        vector<shared_ptr<Dataset>> dataSets = getDatasets(m, N, s);

        for (auto i = 0; i < dataSets.size(); i++) {
            cout << "(" << i << "): " << dataSets[i]->getTitle() << endl;
        }

        int dataSelection;
        while (true) {
            cout << "Choose a data set: ";
            cin >> dataSelection;
            if (cin.fail() || dataSelection < 0 || dataSelection >= dataSets.size()) {
                cin.clear();
                cin.ignore();
                cout << "Invalid selection." << endl;
            } else break;
        }

        return dataSets[dataSelection];
    }

    shared_ptr<Dataset> cli(unsigned int m, unsigned int N, float s, int argc, char** argv) {
        vector<shared_ptr<Dataset>> dataSets = getDatasets(m, N, s);

        for (int i = 0; i < argc; ++i)
            for (auto dataSet : dataSets)
                if (argv[i] == "-" + dataSet->getSlug())
                    return dataSet;

        cerr << "No argument matching data set." << endl;

        return nullptr;
    }

}

#endif //ALGORITHMS_DATASET_SELECTION