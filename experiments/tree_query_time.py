import csv
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
import re
import matplotlib.ticker as plticker

OUTPUT_FILE = './out/tree_query_time.pdf'
RESULTS_FILE_KOSARAK = './results/tree-query-time-kosarak'
RESULTS_FILE_CTU = './results/tree-query-time-ctu'
RESULTS_FILE_ZIPFIAN = './results/tree-query-time-zipfian'
RESULTS_FILE_ZIPFIAN2 = './results/tree-query-time-zipfian-t-39'
RESULTS_FILE_UNIFORM = './results/tree-query-time-uniform'
RESULTS_FILE_NEWSGROUPS = './results/tree-query-time-newsgroups'

plt.style.use('seaborn')

def plot(data):

    plt.figure(figsize=(11, 3))
    plt.grid(True)
    plt.title("Time Complexity for querying Heavy Hitters")
    #plt.suptitle("Tree time complexity")
    plt.tight_layout()

    counter = 1
    for dataset_name, lines in data.items():
        plt.subplot(1,4,counter)
        handles, labels = plot_single(dataset_name, lines, counter==1)
        counter += 1

    plt.subplots_adjust(bottom=0.2)
    plt.gcf().legend(handles[-2:], labels[-2:], ncol=2, loc='lower center')
    plt.savefig(OUTPUT_FILE, bbox_inches='tight')
    plt.clf()

def plot_single(title, data, include_label=True):
    """
    Plot the data an do regression.
    :param name: Title on the plot.
    :param data: Data to plot
    :return:
    """
    styles = ['C5X', 'C5o']
    stylesReg = ['C1-', 'C2-']
    i = 0
    for name, points in data.items():
        points = np.array(points)
        # o = circle, - = solid line between points.
        xs = points[:, 0]
        ys = points[:, 1] / 1000
        ax = plt.plot(xs, ys, styles[i])

        plt.xticks(points[:, 0])

        if(i == 1 or True):
            res = linregress(xs,ys) #slope, intercept, r_value, p_value, std_err
            yp = np.polyval([res[0], res[1]], xs)
            label = "L: ${:.2f}t {} {:.3f}$\n$R^2$: {:.3f}".format(res[0], ("+" if res[1] >= 0 else ""), res[1], res.rvalue*res[2])
            plt.plot(xs, yp, stylesReg[i], label=label)
#            plt.gca().annotate(, xy=(0.03, 0.95), xycoords='axes fraction', size=10, ha='left', va='top')
        i+=1

    handles,labels = plt.gca().get_legend_handles_labels()


    i = 0
    for name, points in data.items():
        plt.plot([], [], styles[i], label=format_legend(name))
        i += 1

    namedDots = plt.gca().get_legend_handles_labels()

    title = title.split("(")[0]
    plt.xlabel('$t$')

    if include_label:
        plt.ylabel('Time in s')
    plt.title(title)
    plt.legend(handles, labels, prop={'size': 7})

    return namedDots

def format_legend(text):
    if "Compact" in text:
        return "Compact"
    else:
        return "Regular"

def analyze_file(filename):
    data = {}

    with open(filename) as results_file:
        results_reader = csv.reader(results_file)

        results_iter = iter(results_reader)

        # Loop through the first few lines of the file containing information about the computer until the line with
        # column labels is reached.
        for labels in results_iter:
            if labels[0] == 'name':
                break

        # Create a label => index map so that it's possible to index into a row by label.
        keys = {k: v for v, k in enumerate(labels)}

        for row in results_iter:
            # Google Benchmark creates a row for several statistics such as mean and median. The name of the statistic
            # is part of the name of the benchmark. We only want to use the median.
            if row[keys['name']].find('median') == -1:
                continue

            # The first part of the name is the actual name of the benchmark. For each benchmark we find we want to
            # store the data for that benchmark, so we make sure we have an initialized array for the data here.
            name = row[keys['name']].split('/')[0]
            if name not in data:
                data[name] = []

            # Append the data entry with the x-value being the t and y being the time spent.
            data[name].append([int(row[keys['t']]), float(row[keys['real_time']]) * pow(10, -6)])

    return data

if __name__ == '__main__':

    data = {}
    data['Zipfian'] = analyze_file(RESULTS_FILE_ZIPFIAN)
    data['Kosarak'] = analyze_file(RESULTS_FILE_KOSARAK)
    data['20 Newsgroups'] = analyze_file(RESULTS_FILE_NEWSGROUPS)
    data['CTU-13'] = analyze_file(RESULTS_FILE_CTU)
    #data['Uniform'] = analyze_file(RESULTS_FILE_UNIFORM)
    #data['Zipfian T39'] = analyze_file(RESULTS_FILE_ZIPFIAN2)

    plot(data)



