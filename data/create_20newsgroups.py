from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer

if __name__ == '__main__':
    # Download the data.
    newsgroups = fetch_20newsgroups(subset='train', remove=('headers', 'footers', 'quotes'))
    data = newsgroups.data

    # Create a CountVectorizer that can help us with properly cleaning an splitting the text data into words (remove
    # whitespace, lowercase, remove punctutation and so on). Also to create a vocabulary of word => unique integer ID.
    # We're not really using it for its intended purpose but are sort of hijacking these two things from it.
    cv = CountVectorizer()
    cv.fit(data)

    # This is a method that will do the word magic.
    analyze = cv.build_analyzer()

    with open('20-newsgroups.dat', 'w') as out:
        # An entry is a post (some text).
        for entry in data:
            # We split it into words with the cleaning and everything.
            for word in analyze(entry):
                # We get the integer representation of each word from the vocabulary.
                out.write(str(cv.vocabulary_.get(word)) + '\n')