#include "streaming_algorithm.h"

#ifndef ALGORITHMS_FREQUENCY_ESTIMATION_ALGORITHM_H
#define ALGORITHMS_FREQUENCY_ESTIMATION_ALGORITHM_H

class FrequencyEstimationAlgorithm : StreamingAlgorithm {
public:
    virtual long long int query(itemKey item) = 0;
};


#endif //ALGORITHMS_FREQUENCY_ESTIMATION_ALGORITHM_H
