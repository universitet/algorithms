#ifndef ALGORITHMS_TEST_UTILS
#define ALGORITHMS_TEST_UTILS

namespace testUtils {

    unsigned int calcL1Norm(std::vector<int> v) {
        unsigned int norm = 0;
        for (const int i : v) {
            norm += abs(i);
        }

        return norm;
    }

    bool estimateInRange(int frequency, float eps, int l1Norm, int estimate) {
        return estimate >= frequency && estimate <= frequency + eps * l1Norm;
    }

};

#endif //ALGORITHMS_TEST_UTILS
