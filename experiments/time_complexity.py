import sys
import csv
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
from scipy.stats import linregress


def plot(ax, name, fmt, data):
    """
    Plot the data an do regression.
    :param name: Title on the plot.
    :param data: Data to plot
    :return:
    """
    regression = linregress(data[:, 0], data[:, 1])

    # Define the regression function.
    def f(x):
        return regression.slope * x + regression.intercept

    ax.plot(data[:, 0], data[:, 1], fmt['points'], label=name)

    ax.xaxis.set_major_locator(ticker.MultipleLocator(base=2))

    # Create some points for the regression function.
    rr = f(np.arange(np.min(data[:, 0]), np.max(data[:, 0]) + 1, data[1, 0] - data[0, 0]))
    # Plot the regression function with the same x-values as the data.
    ax.plot(data[:, 0], rr, fmt['line'], label=f'$R^2 = {np.round(regression.rvalue ** 2, 3)}$')


if __name__ == '__main__':
    plt.style.use('seaborn')
    plt.rcParams.update({'font.size': 11})

    usage_message = 'You must provide a data set parameter (e.g. -zipfian).'

    if not len(sys.argv) >= 2:
        print(usage_message)
        exit(1)

    dataset_name = sys.argv[1][1:]

    data = {}
    with open(f'./out/time-complexity-{dataset_name}') as results_file:
        results_reader = csv.reader(results_file)

        results_iter = iter(results_reader)

        # Loop through the first few lines of the file containing information about the computer until the line with
        # column labels is reached.
        for labels in results_iter:
            if labels[0] == 'name':
                break

        # Create a label => index map so that it's possible to index into a row by label.
        keys = {k: v for v, k in enumerate(labels)}

        for row in results_iter:
            # Google Benchmark creates a row for several statistics such as mean and median. The name of the statistic
            # is part of the name of the benchmark. We only want to use the median.
            if row[keys['name']].find('median') == -1:
                continue

            # The first part of the name is the actual name of the benchmark. For each benchmark we find we want to
            # store the data for that benchmark, so we make sure we have an initialized array for the data here.
            name = row[keys['name']].split('/')[0]
            if name not in data:
                data[name] = []

            # Append the data entry with the x-value being the t and y being the time spent.
            data[name].append([int(row[keys['t']]), float(row[keys['real_time']]) * pow(10, -9)])

    fig, ax = plt.subplots(figsize=(4, 3))

    formats = [
        {
            'points': 'C5X',
            'line': 'C1-',
        },
        {
            'points': 'C5o',
            'line': 'C2-',
        },
    ]
    for fmt, (name, d) in zip(formats, data.items()):
        plot(ax, name[:name.find('\n')], fmt, np.array(d))

    ax.set_ylabel('Time in seconds')
    ax.set_xlabel('$t$')

    title = list(data.keys())[0]
    title = title[title.find('\n') + 2:-1]

    if ',' in title:
        title = title[:title.find(',')]

    ax.set_title(title)

    ax.legend(prop={'size': 7}, ncol=2, loc='upper left')

    fig.savefig(f'./out/time-complexity-{dataset_name}.pdf', bbox_inches='tight')

