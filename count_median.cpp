#include <cmath>
#include "utils/hash_function.h"
#include "count_min.cpp"
#include "utils/median.cpp"

#ifndef ALGORITHMS_COUNT_MEDIAN
#define ALGORITHMS_COUNT_MEDIAN

using namespace std;

class CountMedian : public CountMin {
private:
    HashFunction **signHashFunctions;

protected:
    long long int getCountForBucket(unsigned int row, itemKey item, int count) override {
        return signHashFunctions[row]->hash(item) ? count : -count;
    }

public:
    CountMedian(unsigned int t, unsigned int k) : CountMin(t, k) {
        signHashFunctions = new HashFunction*[t];
        for (unsigned int i = 0; i < t; i++) {
            signHashFunctions[i] = new PrimeHash(2);
        }
    }

    CountMedian(float epsilon, float delta) :
            CountMedian(
                    static_cast<unsigned int>(ceil(log(2.0 / delta))),
                    static_cast<unsigned int>(ceil(exp(1.0) * exp(1.0) * exp(1.0) / (epsilon * epsilon)))) {}

    ~CountMedian() {
        for (unsigned int i = 0; i < t; i++) {
            delete signHashFunctions[i];
        }

        delete[] signHashFunctions;
    }

    long long int query(itemKey item) override {
        // Create a vector to put the result from each row,
        // this allows us to use a library function for finding the median
        std::vector<long long int> values(t);
        for (unsigned int i = 0; i < t; i++) {
            unsigned int hash = hashFunctions[i]->hash(item);

            values[i] = signHashFunctions[i]->hash(item) ? A[i][hash] : -A[i][hash];
        }

        return utils::median(values);
    }
};

#endif //ALGORITHMS_COUNT_MEDIAN
