#include <cstdio>
#include <iostream>
#include "heap.h"
#include "heap_item.cpp"
#include <unordered_map>
#include "../streaming_algorithm.h"

#ifndef ALGORITHMS_MIN_HEAP
#define ALGORITHMS_MIN_HEAP

using namespace std;

using callbackAction = void(*)(HeapItem* item);
/*
 * We maintain the following data-structures:
 *
 *  Min-heap        To store and order by frequency. Easy access to smallest element
 *
 *  Hash table      To store indexes to the items already in the heap. Makes
 *                  amortized O(1) lookup time in min-heap possible
 *
 * NOTE: It might be worth to just expand the hash-map to the full potential size from
 * the very beginning, as it does not have to rehash everything to grow:
 * https://stackoverflow.com/questions/2196995/is-there-any-advantage-of-using-map-over-unordered-map-in-case-of-trivial-keys
 *
 * NOTE: Only works in the cash-register model
 */
class MinHeap : public Heap {
private:
    std::unordered_map<itemKey, int> indexMap;

public:
    explicit MinHeap(int maxSize){
        this->maxSize = maxSize;
    }

    // Lookup item in hash table, if it exists there, it is also contained
    // in the heap and thus, we should update the existing entry
    void put(itemKey item, unsigned int count){
        int index = this->getIndex(item);

        if(index > -1)
            this->update(index, count);
        else
            this->insert(item, count);
    }

    int getSize() override {
        return size;
    }

    unsigned int getMinValue(){
        if(size == 0)
            return 0;
        else
            return items[0].frq;
    }

    // Do not allow modification of node, so return a new one
    // TODO: Maybe just throw exception?
    HeapItem* getRoot() {
        if(size > 0)
            return new HeapItem(items[0].key, items[0].frq);
        else
            return nullptr;
    }

    void print(){
        std::cout << "\n" << " === Heap ===" << "\n";
        for (int i=0; i<size; ++i)
            std::cout << "Index " << i << " (" << items[i].key << "): " << items[i].frq << "\n";
        cout << "\n";
    }

    vector<itemKey> getItemKeys(){
        vector<itemKey> list;
        for (int i = 0; i < size; i++){
            list.push_back(items[i].key);
        }
        return list;
    }
protected:
    int getIndex(itemKey item){
        auto const_iterator = indexMap.find(item);

        // Not found?
        if(const_iterator == indexMap.end()){
            return -1;
        }

        return const_iterator->second;
    }

    void insert(itemKey item, unsigned int count) override {
        // Need delete?
        if(size >= maxSize){
            // Is item too small to be inserted?
            if(count <= this->getMinValue()){
                return;
            }

            // Overwrite root element (same as regular delete in heap)
            indexMap.erase(items[0].key);
            items[0].key = item;
            items[0].frq = count; // Might be negative depending on our assumptions!

            this->sortDown(0);
        } else {
            // Insert as last element
            auto obj = HeapItem(item, count);

            this->items.push_back(obj);
            this->indexMap[item] = size;
            size++;

            this->sortUp(size - 1);
        }
    }

    // We know the item exists, as index is known
    // As the item value increases, the item can only
    // bubble down, so we just sortDown after updating
    void update(int index, unsigned int count) override {
        items[index].frq = count;
        this->sortDown(index);
    }

    // Compare items[child] with its parent. If value is less than parent, then swap.
    // We want the minimum value to be the root
    void sortUp(int child){
        if(child == 0)
            return;

        int parent = (child - 1) / 2;

        // Is parent already smaller than us?
        if(items[parent] < items[child])
            return;

        // Swap!
        swap(parent, child);

        // Continue up in tree
        sortUp(parent);
    }

    // Compare items[parent] with its children (if any). If value is bigger, then swap
    void sortDown(int parent){
        int leftChild = parent * 2 + 1;
        int rightChild = parent * 2 + 2;

        bool hasLeftChild = (leftChild < size);
        bool hasRightChild = (rightChild < size);

        if(hasLeftChild && items[leftChild] < items[parent])
        {
            // RightChild might be even lower than leftChild
            if(hasRightChild && items[rightChild] < items[leftChild])
            {
                // rightChild is the lowest value. Bubble it up
                swap(rightChild, parent);
                sortDown(rightChild);
            } else {
                // leftChild is the lowest value
                swap(leftChild, parent);
                sortDown(leftChild);
            }
            return;
        }

        if(hasRightChild && items[rightChild] < items[parent]){
            swap(rightChild, parent);
            sortDown(rightChild);
        }
    }

    // Swap position of the elements placed at index a and b
    void swap(int a, int b){
        auto itemA = &items[a];
        auto itemB = &items[b];
        auto oldA = HeapItem(itemA->key, itemA->frq);

        // Update hash table
        indexMap[itemA->key] = b;
        indexMap[itemB->key] = a;

        // Update heap using pointers
        itemA->key = itemB->key;
        itemA->frq = itemB->frq;
        itemB->key = oldA.key;
        itemB->frq = oldA.frq;
    }
};

#endif //ALGORITHMS_MIN_HEAP
