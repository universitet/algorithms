#include <vector>
#include <sstream>
#include "../streaming_algorithm.h"
#include "../utils/data_stream.cpp"
#include <stdlib.h>
#include <stdio.h>

#ifndef ALGORITHMS_ZIPFIAN
#define ALGORITHMS_ZIPFIAN

using namespace std;

namespace data {

    static void fill(string fname, unsigned int m, unsigned int n, float s){

        ostringstream cmd;
        cmd << "python3 ../data/generate.py";
        cmd << " \"" << fname << "\"";
        cmd << " " << n;
        cmd << " " << m;
        cmd << " " << s;
        cmd << " -overwrite";
        string fullCmd = cmd.str();
        system(fullCmd.c_str());
    }

    static vector<itemKey> randomizedZipfian(unsigned int m, unsigned int n, float s) {
        string fname = tmpnam(nullptr);
        fill(fname, m, n, s);

        DataStream dataStream = DataStream(fname);
        vector<itemKey> data = dataStream.toVector();

        // Delete file again
        remove(fname.c_str());
        return data;
    }

}

#endif //ALGORITHMS_ZIPFIAN
