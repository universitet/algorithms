#include <iostream>
#include <functional>
#include "benchmark/benchmark.h"
#include "../utils/data_stream.cpp"
#include "../count_min.cpp"
#include "../count_median.cpp"
#include "../utils/random.cpp"
#include "../data/dataset_selection.cpp"

using namespace std;

namespace timeComplexity {

    /**
     * Generate the t and k arguments for the Count Sketch algorithm.
     *
     * @param b
     */
    static void Args(benchmark::internal::Benchmark *b) {
        for (auto t = 2; t <= 20; t += 2) {
            b->Args({t, t * t});
        }
    }

    /**
     * Benchmark the Count Sketch algorithm update operation by going through a stream of data. What is timed is the
     * time it takes to process the entire stream and not each update.
     *
     * @tparam CS
     * @param state
     * @param data
     */
    template<class CS>
    void TimeComplexity(benchmark::State &state, const vector<itemKey> &data) {
        unsigned int t = state.range(0);
        unsigned int k = state.range(1);

        // Save t and k in the output for easy access.
        state.counters.insert({{"t", t},
                               {"k", k}});

        // Create the Count Sketch data structure.
        auto cs = CS(t, k);

        for (auto _ : state) {
            // Iterate through the stream and update.
            for (const auto item : data) {
                cs.update(item, 1);
            }
        }
    }

    /**
     * Register a benchmark with a Count Sketch algorithm.
     *
     * The experiment is conducted as follows. For each t = 1, ..., 10 a Count Sketch data structure is created and the
     * stream is processed. This is repeated 10 times and the median running time is used as the measure for that t.
     */
    template<class CS>
    void Register(const string &name, const vector<itemKey> &data) {
        benchmark::RegisterBenchmark(name.c_str(), &TimeComplexity<CS>, data)
                // We only want to process the stream 1 time.
                ->Iterations(1)
                ->Apply(Args)
                ->Repetitions(5)
                        // Prevents each individual repetition from showing up in the result.
                ->ReportAggregatesOnly(true);
    }
}

using namespace timeComplexity;

int main(int argc, char **argv) {
    const auto m = 50000;
    const auto N = 500000;
    const auto s = 1.07;

    auto dataSet = data::cli(m, N, s, argc, argv);

    if (!dataSet)
        return 1;

    Register<CountMin>(((string) "Count-Min Sketch\n").append("(").append(dataSet->getTitle()).append(")"), dataSet->asVector());
    Register<CountMedian>(((string) "Count-Median Sketch\n").append("(").append(dataSet->getTitle()).append(")"), dataSet->asVector());

    benchmark::Initialize(&argc, argv);
    // Run the benchmarks once to warm up everything and disregard the results of this first run.
    benchmark::RunSpecifiedBenchmarks();
    benchmark::RunSpecifiedBenchmarks();
}