#ifndef ALGORITHMS_DATA_STREAM
#define ALGORITHMS_DATA_STREAM

#include <vector>
#include <iostream>
#include <cstring>
#include "data_stream.h"
#include "../streaming_algorithm.h"

DataStream::DataStream(std::string filename) {
    filestream.open(filename);

    if (!filestream.is_open()) {
        std::cout << "ERROR: Could not open file. Maybe the path is wrong? \n";
    }
}

void DataStream::close() {
    filestream.close();
}


int DataStream::next() {
    if(hasNext()) {
        itemKey item = buffer->back();
        buffer->pop_back();
        return item;
    }

    return -1;
}

bool DataStream::hasNext() {
    if(buffer->empty())
        read();

    return !buffer->empty();
}

void DataStream::read() {

    // Wait until buffer is empty
    if(!buffer->empty())
        return;

    std::string item;
    unsigned long maxItems = DATA_STREAM_BUFFER_SIZE;

    // Read until buffer is full at most
    for (; maxItems > 0; maxItems--) {
        // Read item
        if(!(filestream >> item))
            return;

        // Store it
        buffer->emplace_back(std::stoul(item));
    }
}

std::vector<itemKey> DataStream::toVector() {
    std::vector<itemKey> data;

    while(hasNext())
        data.push_back(next());

    close();

    return data;
}

#endif //ALGORITHMS_DATA_STREAM