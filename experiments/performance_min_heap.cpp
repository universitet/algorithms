#include "../count_heavy.cpp"
#include "../count_min.cpp"
#include "../utils/data_stream.cpp"
#include "../data/zipfian.cpp"
#include "benchmark.cpp"

using namespace std;

void test1(){
    auto heap = CountHeavy<CountMin>(.5f, 0.000001f);
    const auto data = data::randomizedZipfian(20, 20000, 1.07);
    for (const auto item : data)
        heap.update(item, 1);
}

int main() {
    unsigned long iterations = 100;

    auto benchmark = Benchmark();
    for (int i = 0; i < 8; i++)
    {
        benchmark.run(test1, iterations);
        benchmark.printStats();
        iterations *= 2;
    }
}
