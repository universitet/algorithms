#include "../streaming_algorithm.h"

#ifndef ALGORITHMS_HASH_FUNCTION_H
#define ALGORITHMS_HASH_FUNCTION_H

class HashFunction {
public:
    virtual unsigned int hash(itemKey item) = 0;

    virtual ~HashFunction() = default;
};

#endif //ALGORITHMS_HASH_FUNCTION_H
