import csv
import ipaddress

if __name__ == '__main__':
    with open('ctu-13') as raw:
        results_reader = csv.DictReader(raw)

        with open('ctu-13.dat', 'w+') as out:
            for row in results_reader:
                try:
                    out.write(str(int(ipaddress.IPv4Address(row['SrcAddr']))) + '\n')
                    #out.write(str(int(ipaddress.IPv4Address(row['DstAddr']))) + '\n')
                except ValueError as e:
                    print('Could not convert IP address:', e)