#include <cmath>
#include <algorithm>
#include <vector>

#ifndef ALGORITHMS_MEDIAN_H
#define ALGORITHMS_MEDIAN_H

namespace utils {
    static long long int median(std::vector<long long int> &v) {
        auto begin = v.begin();
        auto end = v.end();
        std::size_t size = end - begin;
        std::size_t middleIdx = size / 2;
        auto target = begin + middleIdx;
        std::nth_element(begin, target, end);

        if (size % 2 != 0) {
            // Odd number of elements
            return *target;
        } else {
            // Even number of elements
            double a = *target;
            auto targetNeighbor = target - 1;
            std::nth_element(begin, targetNeighbor, end);
            return ceil((a + *targetNeighbor) / 2.0);
        }
    }

}


#endif //ALGORITHMS_MEDIAN_H
