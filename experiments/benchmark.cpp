
#ifndef ALGORITHMS_BENCHMARK
#define ALGORITHMS_BENCHMARK

#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std::chrono;
using namespace std;

using benchmarkAction = void(*)();

class Benchmark
{
    unsigned long numIterations = 0;
    long maxTime = 0;
    long minTime = 0;
    long totalTime = 0;
    long average = 0;
    long median = 0;
    std::vector<long> iterations;
public:
    Benchmark() : iterations(){

    }

    void run(benchmarkAction function, unsigned long iterations){
        reset(iterations);
        high_resolution_clock::time_point t1, t2;

        // Call it once to clear cache
        function();

        // Run all iterations
        for (unsigned long i = 0; i < iterations; i++)
        {
            t1 = high_resolution_clock::now();
            function();
            t2 = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>( t2 - t1 ).count();
            this->iterations.push_back(duration);
            //cout << "Run " << i << ": " << duration << " mu\n";
            progress(i, iterations);
        }
        progress(iterations, iterations);
        cout << "\n";

        calculate();
    }

    void printStats(){
        cout << "RESULTS";
        cout << "\nIterations: " << numIterations;
        cout << "\nTotal time: "; display_time(totalTime);
        cout << "\nMin time: "; display_time(minTime);
        cout << "\nMax time: "; display_time(maxTime);
        cout << "\nAverage: "; display_time(average);
        cout << "\nMedian: "; display_time(median);
        cout << "\n";
    }
protected:
    void reset(unsigned long num){
        numIterations = num;
        maxTime = 0;
        minTime = 0;
        totalTime = 0;
        average = 0;
        median = 0;
        this->iterations.clear();
        this->iterations.reserve(num);
    }

    void calculate(){
        sort(iterations.begin(), iterations.end());

        minTime = iterations.at(0);
        maxTime = iterations.at(numIterations - 1);
        median = iterations.at(numIterations/2);

        totalTime = 0;
        for (long &it : iterations) {
            totalTime += it;
        }
        average = totalTime / numIterations;
    }

    void display_time(double microseconds)
    {
        if(microseconds > 1000)
        {
            microseconds /= 1000; // Unit is now milliseconds
            if(microseconds > 1000){
                // Seconds
                cout << microseconds / 1000 << " s";
            } else {
                cout << microseconds << " ms";
            }
        } else {

            cout << microseconds << " mu";
        }
    }

    void progress(unsigned long current, unsigned long total)
    {
        double percentage = current * 100 / total;
        int barLength = 50;
        int pos = (percentage / 100) * barLength;

        std::cout << "[";
        for(int i=0; i != barLength; i++)
        {
            if(i < pos)
                std::cout << "=";
            else if(i == pos)
                std::cout << ">";
            else
                std::cout << " ";
        }
        std::cout << "] " << percentage << "%\r" << std::flush;
    }

    void limit_to_one_core()
    {
        /* Linux variant */
        /*
        cpu_set_t my_set;        // Define your cpu_set bit mask.
        CPU_ZERO(&my_set);       // Initialize it all to 0, i.e. no CPUs selected.
        CPU_SET(7, &my_set);     // set the bit that represents core 7.
        sched_setaffinity(0, sizeof(cpu_set_t), &my_set); // Set affinity of tihs process to the defined mask, i.e. only 7.
         */
        /* Windows variant */

    }
};

#endif //ALGORITHMS_HEAP_ITEM