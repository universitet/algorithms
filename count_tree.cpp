#include <cmath>
#include <queue>
#include "heavy_hitters_algorithm.h"
#include "count_min.cpp"

#ifndef ALGORITHMS_COUNT_TREE
#define ALGORITHMS_COUNT_TREE

class CountTree : public HeavyHittersAlgorithm {
private:
    unsigned int totalCount = 0;

    float phi;
    // TODO: What to do about this? Should it be hardcoded? Should it be bigger?.
    //unsigned int size = pow(2, 15);

protected:
    unsigned int levels;
    unsigned long visitedCounter = 0;

    CountMin **sketches;

    virtual void init_sketches(unsigned int t, unsigned int k) const {
        for (int i = 0; i <= levels; i++) {
            sketches[i] = new CountMin(t, k);
        }
    }

    void configure(unsigned int t, unsigned int k, float phi, unsigned int size) {
        this->phi = phi;

        // Calculate the no. of levels in the tree.
        levels = ceil(log2(size));

        // Recalculate the size in case we do rounding.
        size = pow(2, levels);

        sketches = new CountMin *[levels + 1];
    }

    CountTree(){
        // Only to be used by child classes
    }
public:
    CountTree(float phi, float delta, unsigned int size) : CountTree(ceil(log2(1.0f / delta)), ceil(4.0f / phi), phi, size) {}

    CountTree(unsigned int t, unsigned int k, float phi, unsigned int size) {
        configure(t, k, phi, size);

        init_sketches(t, k);
    }

    ~CountTree() {
        // Delete all of the Count-Min Sketches.
        for (int i = 0; i <= levels; i++) {
            delete sketches[i];
        }

        delete[] sketches;
    }

    void update(itemKey item, int count) override {
        totalCount += count;

        for (int i = levels; i >= 0; i--) {
            // Equivalent to floor(item / pow(2, i)).
            sketches[i]->update((item >> i), count);
        }
    }

    unsigned long nodesVisited() {
        return visitedCounter;
    }

    vector<itemKey> heavyHitters() override {
        if (totalCount == 0) {
            return {};
        }

        // Here we save the heavy nodes at the current level. The root node is always heavy (||f|| > phi * ||f||).
        vector<itemKey> heavyNodes = {0};
        visitedCounter = 1;

        // The idea is to go through each level from the root to the leaves. At each level in this loop you know what
        // nodes are heavy. These are the ones whose children we want to look at. We go through each heavy node and
        // identify and save any heavy children of that node. All of these children will be the heavy nodes we wan't to
        // look at at the next level (naturally). When we reach level 1 (the one just above the leaves) we will
        // effectively have identified the heavy hitters since these will be the heavy children.
        for (int i = levels; i >= 0; i--) {
            vector<itemKey> heavyChildren;

            // Go through each of the heavy nodes and find heavy children. Save these to heavyChildren which will be
            // the new heavyNodes in the next iteration.
            for (auto const j: heavyNodes) {
                // You can convince yourself that if we number the nodes at a level 0 through 2^i the left child of a
                // node with no. j at this level will be j * 2 and the right j * 2 + 1.
                itemKey childIndices[2] = {j * 2, j * 2 + 1};
                for (auto const childIndex: childIndices) {
                    int f = sketches[i - 1]->query(childIndex);

                    if (f >= phi * totalCount) {
                        heavyChildren.push_back(childIndex);
                    }
                }
            }

            visitedCounter += heavyChildren.size();

            // If at any point there are no heavy nodes in the next level there are no heavy nodes at all and we are
            // done.
            if (heavyChildren.empty()) {
                return {};
            }

            // When we reach level 1 (the one just above the leaves) we will effectively have identified the heavy
            // hitters since these will be the heavy children.
            if (i == 1) {
                return heavyChildren;
            }

            heavyNodes = heavyChildren;
        }

        // Just here to stop the IDE complaining.
        return {};
    }
};

#endif //ALGORITHMS_COUNT_TREE
