#include "../count_min.cpp"
#include "../test/test_utils.cpp"

using namespace std;
using namespace testUtils;

int main() {
    const int iterations = 1000;
    // Size of frequency vector.
    const int freqSize = 1000;

    int failures = 0;

    const float epsilon = .1f;
    const float delta = .05f;

    printf(
            "Testing Count-Min Sketch with epsilon = %f and delta = %f and with a frequency vector of size %d, %d times.\n",
            epsilon, delta, freqSize, iterations
    );

    // The total number of failures should be <= Pr[fail] * no. of attempts. (which is the no. of iterations multiplied
    // by the size of the frequency vector since for each iteration we test each element).
    printf("The expected no. of failures is %f.\n", delta * iterations * freqSize);

    // Test Count-Min many times. Generate a random frequency vector, estimate frequencies with CM and check that the
    // frequency of a random item is within range.
    for (int count = 0; count < iterations; count++) {
        vector<int> frequencies(freqSize);

        // Generate random frequency vector.
        for (int i = 0; i < frequencies.size(); i++) {
            frequencies[i] = rand_int(0, 1000);
        }

        unsigned int norm = calcL1Norm(frequencies);

        // Create the CM sketch and perform one update for each item with the correct frequency. This is the same as
        // e.g. seeing many (5, 1), (5, 6), (5, 2), ... and doing an update with each of them.
        auto *cm = new CountMin(epsilon, delta);
        for (int i = 0; i < frequencies.size(); i++) {
            cm->update(i, frequencies[i]);
        }

        // Check that the estimate of items is within range. If not increment no. of failures.
        for (int i = 0; i < frequencies.size(); i++) {
            if (!estimateInRange(frequencies[i], epsilon, norm, cm->query(i))) {
                failures++;
            }
        }
    }

    printf("The actual no. of failures is %d.\n", failures);
}
