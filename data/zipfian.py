import numpy as np
import scipy.stats as stats


def _apply_permutation(m, pool):
    x = np.arange(1, m + 1)
    x = np.random.permutation(x)

    return [x[val - 1] for val in pool]


def zipfian(m, n, s):
    # Zipfian
    # See url https://stackoverflow.com/questions/33331087/sampling-from-a-bounded-domain-zipf-distribution

    x = np.arange(1, m + 1)
    weights = x ** (-s)
    weights /= weights.sum()
    bounded_zipf = stats.rv_discrete(name='bounded_zipf', values=(x, weights))

    return list(bounded_zipf.rvs(size=n))


def randomized_zipfian(m, n, s):
    return _apply_permutation(m, zipfian(m, n, s))
